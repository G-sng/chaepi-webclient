# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM tiangolo/node-frontend:10 as build-stage

# ENV NPM_CONFIG_LOGLEVEL warn
# ENV APP_ENV production

RUN mkdir -p /chaepi-webclient
WORKDIR /chaepi-webclient

COPY ./package*.json ./

RUN npm install

COPY . ./

RUN npm run build

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.15

COPY --from=build-stage /chaepi-webclient/build/ /usr/share/nginx/html
# Copy the default nginx.conf provided by tiangolo/node-frontend
COPY --from=build-stage /nginx.conf /etc/nginx/conf.d/default.conf
