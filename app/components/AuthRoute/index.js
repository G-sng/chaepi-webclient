/**
 *
 * AuthRoute
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { Redirect, Route } from 'react-router-dom';

function AuthRoute({ component: Component, isAuthenticated, ...otherProps }) {
  return (
    <Route
      {...otherProps}
      render={props =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: {
                from: props.location,
                error:
                  'Your access grant has expired or missing, please login again!',
              },
            }}
          />
        )
      }
    />
  );
}

AuthRoute.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  component: PropTypes.any,
  location: PropTypes.any,
};

export default AuthRoute;
