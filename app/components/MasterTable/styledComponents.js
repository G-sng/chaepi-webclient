import styled from 'styled-components';
import dreColors from 'themes/dre';

export const styledComponents = {
  Wrapper: styled.div`
    h1 {
      color: ${dreColors.blueButton};
      font-size: 24px;
      font-weight: 400;
      min-width: 150px;
    }
    h4 {
      font-weight: 400;
      text-transform: uppercase;
      color: ${dreColors.blueButton};
    }
    h3 {
      color: ${dreColors.blueButton};
    }
  `,

  Table: styled.div`
    table {
      min-width: 100%;
      border-collapse: separate;
      border-spacing: 0 15px;
      color: #808080;
      font-size: 12px;
      text-align: left;
    }

    thead {
      margin-bottom: 20px;
      background: ${dreColors.blueButton};

      th {
        height: 60px;
        // padding: 20px;
        text-align: center;
        color: #fff;
        font-size: 120%;
        font-weight: 500;
        border: 0px solid transparent;

        &:first-child {
          border-left-style: solid;
          border-top-left-radius: 8px;
          border-bottom-left-radius: 8px;
        }
        &:last-child {
          border-right-style: solid;
          border-bottom-right-radius: 8px;
          border-top-right-radius: 8px;
        }
      }
    }

    tbody {
      tr {
        height: 50px;
        cursor: pointer;
        transform: perspective(1px) translateZ(0);
        transition-duration: 0.3s;
        transition-property: box-shadow, transform;
        box-shadow: 0 0 1px transparent;

        &:hover {
          border-radius: 10px;
          box-shadow: 0 4px 10px 1px rgba(0, 0, 0, 0.3);
          transform: scale(1.01);
        }
      }
      .no-hover {
        cursor: default;
        &:hover {
          border-radius: 10px;
          box-shadow: none;
          transform: none;
        }
      }
    }

    td {
      text-align: center;
      position: relative;
      padding: 10px;
      border: 0px solid transparent;
      background: #fff;

      &:first-child {
        border-left-style: solid;
        border-top-left-radius: 8px;
        border-bottom-left-radius: 8px;
      }
      &:last-child {
        padding-bottom: 10px;
        display: table-cell;
        /* vertical-align: bottom; */
        border-right-style: solid;
        border-bottom-right-radius: 8px;
        border-top-right-radius: 8px;
      }
    }
  `,
};
