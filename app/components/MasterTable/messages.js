/*
 * MasterTable Messages
 *
 * This contains all the text for the MasterTable component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.MasterTable';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the MasterTable component!',
  },
});
