/**
 *
 * MasterTable
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';
import { styledComponents } from './styledComponents'; // styled divs

function MasterTable(props) {
  const { Wrapper, Table } = styledComponents;
  const {
    columns,
    data,
    disableHover,
    handleRenderCell,
    handleRowItemClick = () => null,
  } = props;

  return (
    <Wrapper
      style={{
        marginTop: 15,
        height: 'inherit',
      }}
    >
      <Table>
        <table>
          <thead>
            <tr>
              {columns.map(column => (
                <th
                  key={column.sort}
                  colSpan={column.size > 1 ? column.size : null}
                >
                  {column.name}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {data.map((row, key) => (
              <tr
                key={key}
                onClick={() => handleRowItemClick(row)}
                className={disableHover ? 'no-hover' : ''}
              >
                {columns.map(column => (
                  <td
                    key={column.sort}
                    colSpan={column.size > 1 ? column.size : null}
                  >
                    {handleRenderCell(column.type, row)}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </Table>
      {/* {data.length === 0 && (
        <div style={{ maxWidth: '250px', margin: 'auto' }}>
          <img
            alt="no-item"
            style={{ maxWidth: '250px' }}
            className="loadingLogo"
            src=""
          />
        </div>
      )} */}
    </Wrapper>
  );
}

MasterTable.propTypes = {
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  disableHover: PropTypes.bool,
  handleRenderCell: PropTypes.func.isRequired,
  handleRowItemClick: PropTypes.func,
};

export default MasterTable;
