/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/**
 *
 * ThumbnailGallery
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  min-height: 10%;
  height: 10%;
  max-height: 10%;
  margin: 0 10px;
  overflow: auto;
  .thumbnail-images {
    cursor: pointer;
    max-height: 55px;
    max-width: 55px;
    margin: auto 12px;
    padding: 1px;
  }
  .thumbnail-images.selected {
    border: 3px solid #3f51b5;
  }
`;

function ThumbnailGallery({ images, handleSelectImage, selectedImage }) {
  return (
    <Wrapper>
      {images.map(item => {
        const imageStyle = {};
        if (item.status === 'IMAGE_REJECTED') {
          imageStyle.borderTop = '10px solid #f50057';
        } else if (item.status === 'PENDING_REVIEW') {
          imageStyle.borderTop = '10px solid green';
        } else if (item.status === 'IN_PROGRESS') {
          imageStyle.borderTop = '10px solid #3f51b5';
        }
        return (
          <img
            key={item.id}
            className={
              selectedImage.id === item.id
                ? 'thumbnail-images selected'
                : 'thumbnail-images'
            }
            style={imageStyle}
            src={item.signedUrl}
            alt="thumbnail"
            onClick={() => handleSelectImage(item)}
          />
        );
      })}
    </Wrapper>
  );
}

ThumbnailGallery.propTypes = {
  images: PropTypes.array.isRequired,
  handleSelectImage: PropTypes.func.isRequired,
  selectedImage: PropTypes.object.isRequired,
};

export default ThumbnailGallery;
