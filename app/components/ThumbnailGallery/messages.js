/*
 * ThumbnailGallery Messages
 *
 * This contains all the text for the ThumbnailGallery component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.ThumbnailGallery';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ThumbnailGallery component!',
  },
});
