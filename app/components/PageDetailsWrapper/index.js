/**
 *
 * PageDetailsWrapper
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  margin: 0px 5px 10px;
  height: 100%;
`;

function PageDetailsWrapper({ children }) {
  return <Wrapper>{children}</Wrapper>;
}

PageDetailsWrapper.propTypes = {
  children: PropTypes.node.isRequired,
};

export default PageDetailsWrapper;
