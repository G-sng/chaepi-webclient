import React from 'react';
// import PropTypes from 'prop-types';

import Select from '@material-ui/core/Select';
// import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';

// const renderFromHelper = ({ touched, error }) => {
//   if (touched && error) {
//     return <FormHelperText>{touched && error}</FormHelperText>;
//   }
//   return null;
// };

export const renderSelectField = ({
  input,
  label,
  meta: { touched, error },
  children,
  ...custom
}) => (
  <FormControl
    fullWidth
    required={custom.required || false}
    className={custom.formcontrolclassname}
    error={touched && error}
    style={{ marginTop: '1em' }}
  >
    <InputLabel
      className={custom.inputlabelclassname}
      shrink
      htmlFor={custom.inputid}
    >
      {label}
    </InputLabel>
    <Select
      {...input}
      {...custom}
      input={<Input name={custom.inputname} id={custom.inputid} />}
    >
      {children}
    </Select>
    {/* {renderFromHelper({ touched, error })} */}
  </FormControl>
);

// renderSelectField.propTypes = {
//   touched: PropTypes.any,
//   error: PropTypes.any,
// };

export default renderSelectField;

// sample field
//
// {/* <Field
//   component={SelectField}
//   label="INCIDENT TYPE"
//   // value={this.state.insuranceCompanyName}
//   // onChange={this.handleChange}
//   inputname="incidentType"
//   inputid="incident-type"
//   // displayEmpty
//   name="incidentType"
//   formcontrolclassname={classes.formControl}
//   inputlabelclassname={classes.inputLabel}
//   className={classes.selectEmpty}
//   classes={{ select: classes.inputElement }}
// >
//   <MenuItem className={classes.menuItem} value="">
//     <em>None</em>
//   </MenuItem>
//   <MenuItem className={classes.menuItem} value={10}>
//     Ten
//   </MenuItem>
//   <MenuItem className={classes.menuItem} value={20}>
//     Twenty
//   </MenuItem>
//   <MenuItem className={classes.menuItem} value={30}>
//     Thirty
//   </MenuItem>
// </Field> */}
