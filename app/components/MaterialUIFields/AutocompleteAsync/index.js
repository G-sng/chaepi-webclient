import React from 'react';
import PropTypes from 'prop-types';
// import Select from 'react-select';
import Async from 'react-select/async';

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  input: {
    display: 'flex',
    padding: 0,
    height: 'auto',
  },
  placeholder: {
    position: 'absolute',
    // left: 2,
    // bottom: 6,
    fontSize: 16,
    opacity: '0.5',
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing(1),
    left: 0,
    right: 0,
  },
}));

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

inputComponent.propTypes = {
  inputRef: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
};

function Control(props) {
  const {
    children,
    innerProps,
    innerRef,
    selectProps: { classes, TextFieldProps },
  } = props;

  return (
    <TextField
      fullWidth
      id="material-text-field"
      margin="normal"
      InputProps={{
        inputComponent,
        inputProps: {
          className: classes.input,
          ref: innerRef,
          children,
          ...innerProps,
        },
        // ...props.selectProps.InputProps,
      }}
      {...TextFieldProps}
    />
  );
}

Control.propTypes = {
  children: PropTypes.node,
  innerProps: PropTypes.object,
  innerRef: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  selectProps: PropTypes.object.isRequired,
};

function Option(props) {
  return (
    <MenuItem
      buttonRef={props.innerRef}
      selected={props.isFocused}
      component="div"
      style={{
        fontWeight: props.isSelected ? 500 : 400,
        fontSize: '14px',
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

Option.propTypes = {
  children: PropTypes.node,
  innerProps: PropTypes.object,
  innerRef: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  isFocused: PropTypes.bool,
  isSelected: PropTypes.bool,
};

function Menu(props) {
  return (
    <Paper
      square
      className={props.selectProps.classes.paper}
      {...props.innerProps}
    >
      {props.children}
    </Paper>
  );
}

Menu.propTypes = {
  children: PropTypes.node,
  innerProps: PropTypes.object,
  selectProps: PropTypes.object,
};

function Placeholder(props) {
  return (
    <div
      color="textSecondary"
      className={props.selectProps.classes.placeholder}
      {...props.innerProps}
    >
      {props.selectProps.TextFieldProps.placeholder || ''}
    </div>
  );
}

Placeholder.propTypes = {
  children: PropTypes.node,
  innerProps: PropTypes.object,
  selectProps: PropTypes.object.isRequired,
};

const components = {
  Control,
  Menu,
  Option,
  Placeholder,
};

export const RenderAutocompleteAsync = ({
  input,
  meta: { touched, error },
  ...custom
}) => {
  const classes = useStyles();

  const selectStyles = {
    input: base => ({
      ...base,
      // color: theme.palette.text.primary,
      '& input': {
        font: 'inherit',
      },
    }),
  };
  return (
    <FormControl
      fullWidth
      required={custom.required || false}
      className={custom.formcontrolclassname}
      error={touched && error}
      style={{ marginTop: '1em' }}
    >
      <Async
        classes={classes}
        styles={selectStyles}
        TextFieldProps={custom.TextFieldProps}
        // options={custom.suggestions}
        components={components}
        {...input}
        {...custom}
        // onChange={value => input.onChange(value)}
        onBlur={() =>
          // to stop field getting cleared on blur
          console.log('---->  ', input) && input.onBlur(input.value)
        }
      />
    </FormControl>
  );
  // return (
  //   <Async
  //     error={touched && invalid}
  //     helperText={touched && error}
  //     {...input}
  //     {...custom}
  //     styles={selectStyles}
  //     components={components}
  //   />
  // );
};

RenderAutocompleteAsync.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
};

export default RenderAutocompleteAsync;

// sample field
//
// {/* <Field
//    name="make"
//    id="make"
//    component={AutocompleteAsync}
//    cacheOptions
//    defaultOptions
//    loadOptions={handleMakeOptions}
//    TextFieldProps={{
//      label: 'Make',
//      InputLabelProps: {
//        shrink: true,
//      },
//      placeholder: 'Select Make',
//    }}
//    onInputChange={handleFetchMotorMakeSuggestions}
//    onChange={val => handleSaveFormValues('make', val)}
//    /> */}

// function handleMakeOptions() {
//   const vehiclesOptions = motorMakeSuggestions.map(item => ({
//     label: item.make,
//     value: item.make,
//   }));
//   // eslint-disable-next-line no-new
//   return new Promise(resolve => {
//     resolve(vehiclesOptions);
//   });
// }
