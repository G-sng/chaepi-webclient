import React from 'react';
import PropTypes from 'prop-types';

import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

export const renderRadioGroup = ({
  children,
  input,
  label,
  // meta: { touched, error },
  ...rest
}) => (
  <FormControl>
    {!!label && (
      <FormLabel
        style={{ fontSize: '14px', margin: '10px 0px 0px' }}
        component="legend"
      >
        {label}
      </FormLabel>
    )}
    <RadioGroup
      {...input}
      {...rest}
      style={{ display: 'flex', flexDirection: 'row' }}
    >
      {children}
    </RadioGroup>
  </FormControl>
);

renderRadioGroup.propTypes = {
  children: PropTypes.node.isRequired,
  input: PropTypes.object.isRequired,
  // meta: PropTypes.object.isRequired,
  label: PropTypes.string,
};

export default renderRadioGroup;
