/**
 *
 * PasswordLoginForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Field, reduxForm } from 'redux-form';

import MenuItem from '@material-ui/core/MenuItem';

import Button from 'components/MaterialUIFields/Button';
import SelectField from 'components/MaterialUIFields/SelectField';
import TextField from 'components/MaterialUIFields/TextField';

import { ASSET_URL, roles } from 'utils/helper';
import { toTitleCase } from 'utils/helperFunctions';
// import dreColors from 'themes/dre';

import validate from './validate';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

const Wrapper = styled.div`
  .input-field {
    margin: 12px 0px;
  }
  .login-footer {
    margin: 30px 0;
    margin-bottom: 10px;
  }
`;

function PasswordLoginForm({ loading, handleSubmit }) {
  const loginButtonClasses = {
    // root: {
    //   // backgroundColor: dreColors.blueButton,
    //   color: dreColors.whiteText,
    //   '&:hover': {
    //     // backgroundColor: dreColors.blueButtonHover,
    //   },
    // },
  };

  return (
    <Wrapper>
      <form onSubmit={handleSubmit}>
        <Field
          className="input-field"
          name="role"
          label="Role"
          required
          component={SelectField}
        >
          {roles.map((item, key) => (
            <MenuItem value={item} key={key}>
              {toTitleCase(item)}
            </MenuItem>
          ))}
        </Field>
        <Field
          className="input-field"
          name="email"
          label="Email"
          required
          component={TextField}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <Field
          className="input-field"
          name="password"
          type="password"
          label="Password"
          required
          component={TextField}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <div className="login-footer">
          {loading ? (
            <Button fullWidth buttonclasses={loginButtonClasses}>
              <img
                alt="loader"
                style={{ width: '30px' }}
                src={`${ASSET_URL}/loader_white.gif`}
              />
            </Button>
          ) : (
            <Button fullWidth type="submit" buttonclasses={loginButtonClasses}>
              SIGN IN
            </Button>
          )}
        </div>
      </form>
    </Wrapper>
  );
}

PasswordLoginForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'passwordLoginForm',
  validate,
})(PasswordLoginForm);
