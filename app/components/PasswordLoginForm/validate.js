function validate(values) {
  const errors = {};

  if (!values.role) {
    errors.role = 'Required';
  }
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }
  if (!values.password) {
    errors.password = 'Required';
  } else if (
    !/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/i.test(
      values.password,
    )
  ) {
    errors.password =
      'Password should be minimum 8 characters long with atleast 1 capital letter, 1 small letter, 1 special character and 1 numeric digit';
  }

  return errors;
}

export default validate;
