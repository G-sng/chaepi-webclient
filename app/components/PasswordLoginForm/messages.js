/*
 * PasswordLoginForm Messages
 *
 * This contains all the text for the PasswordLoginForm component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.PasswordLoginForm';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the PasswordLoginForm component!',
  },
});
