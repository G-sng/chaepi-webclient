/**
 *
 * RejectImageDialog
 *
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import Button from 'components/MaterialUIFields/Button';

import { motorRejectionReasonSuggestions } from 'utils/helper';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

const Wrapper = styled.div``;

function RejectImageDialog({ handleSubmitRejectionReason }) {
  const [rejectionReason, setRejectionReason] = useState('');
  const [showRejectionDialog, setShowRejectionDialog] = useState(false);

  function handleCloseRejectionModal() {
    setShowRejectionDialog(false);
    setRejectionReason('');
  }

  function handleSubmitReason() {
    if (rejectionReason) {
      handleSubmitRejectionReason(rejectionReason);
    } else {
      alert('Please select a Reason for Rejection');
    }
  }

  return (
    <Wrapper>
      <Button
        className="reject-image"
        fullWidth
        buttonclasses={{}}
        color="secondary"
        onClick={() => setShowRejectionDialog(true)}
      >
        REJECT IMAGE
      </Button>
      <Dialog
        fullWidth
        open={showRejectionDialog}
        onClose={handleCloseRejectionModal}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Reason for Rejection</DialogTitle>
        <DialogContent>
          <InputLabel htmlFor="age-simple">Reason</InputLabel>
          <Select
            fullWidth
            value={rejectionReason}
            onChange={e => setRejectionReason(e.target.value)}
            inputProps={{
              name: 'age',
              id: 'age-simple',
            }}
          >
            {motorRejectionReasonSuggestions.map((item, key) => (
              <MenuItem key={key} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseRejectionModal} color="secondary">
            Cancel
          </Button>
          <Button onClick={handleSubmitReason} color="primary">
            Reject
          </Button>
        </DialogActions>
      </Dialog>
    </Wrapper>
  );
}

RejectImageDialog.propTypes = {
  handleSubmitRejectionReason: PropTypes.func.isRequired,
};

export default RejectImageDialog;
