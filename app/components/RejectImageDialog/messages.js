/*
 * RejectImageDialog Messages
 *
 * This contains all the text for the RejectImageDialog component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.RejectImageDialog';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RejectImageDialog component!',
  },
});
