function submit(values, dispatch, props) {
  const { handleClosePanel, handleSubmitDamage, nameOfClass } = props;

  if (values.damageType && values.severity && values.action) {
    const marking = {
      class: values.damageType,
      severity: values.severity,
      action: values.action,
      parentClass: nameOfClass,
    };
    handleSubmitDamage(marking);
    handleClosePanel();
  }
}
export default submit;
