/*
 * MotorActionPanel Messages
 *
 * This contains all the text for the MotorActionPanel component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.MotorActionPanel';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the MotorActionPanel component!',
  },
  profile: {
    id: `${scope}.profile`,
    defaultMessage: 'PROFILE',
  },
  parts: {
    id: `${scope}.parts`,
    defaultMessage: 'PARTS',
  },
  damages: {
    id: `${scope}.damages`,
    defaultMessage: 'DAMAGES',
  },
});
