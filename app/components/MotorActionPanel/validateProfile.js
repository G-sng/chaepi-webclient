function validate(values) {
  const errors = {};

  if (!values.profile) {
    errors.profile = 'required';
  }
  if (!values.visibility) {
    errors.visibility = 'required';
  }

  return errors;
}

export default validate;
