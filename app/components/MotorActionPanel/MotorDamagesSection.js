/**
 *
 * MotorDamagesSection
 *
 */

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import IconButton from '@material-ui/core/IconButton';

import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ClearIcon from '@material-ui/icons/Clear';
// import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import Button from 'components/MaterialUIFields/Button';

// import { ASSET_URL } from 'utils/helper';
import { toTitleCase } from 'utils/helperFunctions';

import MotorDamagesVisibilityForm from './MotorDamagesVisibilityForm';

// import validate from './validate';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

const Wrapper = styled.div`
  margin: 10px;
  .input-field {
    margin: 12px 0px;
  }
  .login-footer {
    margin: 30px 0;
    margin-bottom: 10px;
  }
  .expansion-panels {
    margin: 20px 5px 10px;
    .profile-expansion-panel {
      .panel {
        margin: 10px 5px;
        .expansion-panel-summary {
          display: flex;
          .panel-description-text {
            flex: 1;
            align-self: center;
          }
          .action-successful {
            color: green;
          }
        }
        .visibility-form-details {
          flex-direction: column;
          padding: 0px 5px;
        }
      }
    }
  }
`;

function MotorDamagesSection({
  handleFetchMotorDamagesSuggestions,
  handleRemoveMotorDamagesSuggestion,
  handleSaveDamagesFormValues,
  handleSubmitImage,
  handleSubmitDamage,
  motorDamagesSuggestions,
  selectedImage,
}) {
  const [expandedPanel, setExpandedPanel] = useState(false);

  useEffect(() => {
    handleFetchMotorDamagesSuggestions();
  }, []);

  function handleRemoveItem(e, damage, key) {
    e.stopPropagation();
    handleRemoveMotorDamagesSuggestion(damage, key);
  }

  return (
    <Wrapper>
      <div className="expansion-panels">
        <div className="profile-expansion-panel">
          {motorDamagesSuggestions.map((item, key) => {
            const existingPolygons =
              selectedImage.reverseDamagesPolygons[item.class] || [];
            const damageClasses = selectedImage.damageClasses[item.class] || [];
            return (
              <ExpansionPanel
                key={item.class}
                className="panel"
                expanded={expandedPanel === item.class}
                onChange={(e, isExpanded) =>
                  setExpandedPanel(isExpanded ? item.class : false)
                }
              >
                <ExpansionPanelSummary
                  className="expansion-panel-summary"
                  // expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <div className="panel-description-text">
                    {toTitleCase(item.class)}
                  </div>
                  {existingPolygons.length > 0 && (
                    <IconButton
                      className="action-successful"
                      variant="contained"
                    >
                      <CheckCircleIcon />
                    </IconButton>
                  )}
                  <IconButton
                    variant="contained"
                    color="secondary"
                    onClick={e => handleRemoveItem(e, item, key)}
                  >
                    <ClearIcon />
                  </IconButton>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails className="visibility-form-details">
                  <MotorDamagesVisibilityForm
                    damageClasses={damageClasses}
                    form={`motorDamage${item.class}Form`}
                    handleClosePanel={() => setExpandedPanel(false)}
                    handleSaveDamagesFormValues={handleSaveDamagesFormValues}
                    handleSubmitDamage={handleSubmitDamage}
                    index={key}
                    initialValues={item.motorDamageFormValue}
                    loading={false}
                    nameOfClass={item.class}
                  />
                </ExpansionPanelDetails>
              </ExpansionPanel>
            );
          })}
        </div>
      </div>
      <div className="login-footer">
        {/* {loading ? (
          <Button fullWidth buttonclasses={{}}>
            <img
              alt="loader"
              style={{ width: '30px' }}
              src={`${ASSET_URL}/loader_white.gif`}
            />
          </Button>
        ) : (
          <Button fullWidth type="submit" buttonclasses={{}}>
            NEXT
          </Button>
        )} */}
        <Button
          onClick={handleSubmitImage} // finish marking the image
          fullWidth
          buttonclasses={{}}
        >
          Submit Image
        </Button>
      </div>
    </Wrapper>
  );
}

MotorDamagesSection.propTypes = {
  // loading: PropTypes.bool.isRequired,
  handleFetchMotorDamagesSuggestions: PropTypes.func.isRequired,
  handleRemoveMotorDamagesSuggestion: PropTypes.func.isRequired,
  handleSaveDamagesFormValues: PropTypes.func.isRequired,
  handleSubmitImage: PropTypes.func.isRequired,
  handleSubmitDamage: PropTypes.func.isRequired,
  // handleSubmit: PropTypes.func.isRequired,
  motorDamagesSuggestions: PropTypes.array.isRequired,
  selectedImage: PropTypes.object.isRequired,
};

export default MotorDamagesSection;
