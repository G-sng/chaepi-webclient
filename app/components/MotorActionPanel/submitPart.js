function submit(values, dispatch, props) {
  const { handleClosePanel, handleSubmitPart, nameOfClass } = props;

  if (values.isDamagedTextual && values.visibility) {
    const marking = {
      isDamaged: values.isDamaged,
      isDamagedTextual: values.isDamagedTextual,
      visibility: values.visibility,
      class: nameOfClass,
    };
    handleSubmitPart(marking);
    handleClosePanel();
  }
}
export default submit;
