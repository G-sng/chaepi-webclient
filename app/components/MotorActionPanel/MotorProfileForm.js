/**
 *
 * MotorProfileForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Field, reduxForm } from 'redux-form';

import Autocomplete from 'components/MaterialUIFields/Autocomplete';
import AutocompleteAsync from 'components/MaterialUIFields/AutocompleteAsync';
import Button from 'components/MaterialUIFields/Button';

import { ASSET_URL, motorVisibilitySuggestions } from 'utils/helper';
import { toTitleCase } from 'utils/helperFunctions';

// import validate from './validateProfile';
import submit from './submitProfile';
import RejectImageDialog from '../RejectImageDialog';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

const Wrapper = styled.div`
  margin: 10px;
  .input-field {
    margin: 12px 0px;
  }
  .login-footer {
    margin: 30px 0;
    margin-bottom: 10px;
    .reject-image {
      margin-top: 10px;
    }
  }
`;

function MotorProfileForm({
  loading,
  handleFetchMotorMakeSuggestions,
  handleFetchMotorModelSuggestions,
  handleSaveFormValues,
  handleSubmit,
  handleSubmitRejectionReason,
  motorMakeSuggestions,
  motorModelSuggestions,
  motorProfilesSuggestions,
}) {
  function handleMakeOptions() {
    const vehiclesOptions = motorMakeSuggestions.map(item => ({
      label: item.make,
      value: item.make,
    }));
    // eslint-disable-next-line no-new
    return new Promise(resolve => {
      resolve(vehiclesOptions);
    });
  }

  function handleModelOptions() {
    const vehiclesOptions = motorModelSuggestions.map(item => ({
      label: item.model,
      value: item.model,
    }));
    // eslint-disable-next-line no-new
    return new Promise(resolve => {
      resolve(vehiclesOptions);
    });
  }

  function handleProfilesSuggestions() {
    return motorProfilesSuggestions.map(item => ({
      label: toTitleCase(item.class),
      value: item.class,
    }));
  }

  return (
    <Wrapper>
      <form onSubmit={handleSubmit}>
        <Field
          name="make"
          id="make"
          component={AutocompleteAsync}
          cacheOptions
          defaultOptions
          loadOptions={handleMakeOptions}
          TextFieldProps={{
            label: 'Make',
            InputLabelProps: {
              shrink: true,
            },
            placeholder: 'Select Make',
          }}
          onInputChange={handleFetchMotorMakeSuggestions}
          onChange={val => handleSaveFormValues('make', val)}
        />
        <Field
          name="model"
          id="model"
          component={AutocompleteAsync}
          cacheOptions
          defaultOptions
          loadOptions={handleModelOptions}
          TextFieldProps={{
            label: 'Model',
            InputLabelProps: {
              shrink: true,
            },
            placeholder: 'Select Model',
          }}
          onInputChange={handleFetchMotorModelSuggestions}
          onChange={val => handleSaveFormValues('model', val)}
        />
        <Field
          name="profile"
          id="profile"
          component={Autocomplete}
          suggestions={handleProfilesSuggestions()}
          TextFieldProps={{
            label: 'Profile',
            InputLabelProps: {
              shrink: true,
            },
            placeholder: 'Select Profile',
            required: true,
          }}
          onChange={val => handleSaveFormValues('profile', val)}
        />
        <Field
          name="visibility"
          id="visibility"
          component={Autocomplete}
          suggestions={motorVisibilitySuggestions}
          TextFieldProps={{
            label: 'Visibility',
            InputLabelProps: {
              shrink: true,
            },
            placeholder: 'Select Visibility',
            required: true,
          }}
          onChange={val => handleSaveFormValues('visibility', val)}
        />
        <div className="login-footer">
          {loading ? (
            <Button fullWidth buttonclasses={{}}>
              <img
                alt="loader"
                style={{ width: '30px' }}
                src={`${ASSET_URL}/loader_white.gif`}
              />
            </Button>
          ) : (
            <Button fullWidth type="submit" buttonclasses={{}}>
              SAVE
            </Button>
          )}
        </div>
        <RejectImageDialog
          handleSubmitRejectionReason={handleSubmitRejectionReason}
        />
      </form>
    </Wrapper>
  );
}

MotorProfileForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  handleFetchMotorMakeSuggestions: PropTypes.func.isRequired,
  handleFetchMotorModelSuggestions: PropTypes.func.isRequired,
  handleSaveFormValues: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleSubmitRejectionReason: PropTypes.func.isRequired,
  motorMakeSuggestions: PropTypes.array.isRequired,
  motorModelSuggestions: PropTypes.array.isRequired,
  motorProfilesSuggestions: PropTypes.array.isRequired,
};

export default reduxForm({
  enableReinitialize: true,
  form: 'motorProfileForm',
  // validate,
  onSubmit: submit,
})(MotorProfileForm);
