/**
 *
 * MotorPartsVisibilityForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Field, reduxForm } from 'redux-form';

import Autocomplete from 'components/MaterialUIFields/Autocomplete';
import Button from 'components/MaterialUIFields/Button';
import RadioGroup from 'components/MaterialUIFields/RadioGroup';

import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import { ASSET_URL, motorVisibilitySuggestions } from 'utils/helper';
// import { toTitleCase } from 'utils/helperFunctions';

// import validate from './validateProfile';
import submit from './submitPart';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

const Wrapper = styled.div`
  margin: -15px 10px 10px;
  .input-field {
    margin: 12px 0px;
  }
  .footer {
    margin: 10px 0px;
    margin-bottom: 10px;
  }
`;

function MotorPartsVisibilityForm({
  handleSavePartsFormValues,
  handleSubmit,
  index,
  loading,
}) {
  return (
    <Wrapper>
      <form onSubmit={handleSubmit}>
        <Field
          name="visibility"
          id="visibility"
          component={Autocomplete}
          suggestions={motorVisibilitySuggestions}
          TextFieldProps={{
            label: 'Visibility',
            InputLabelProps: {
              shrink: true,
            },
            placeholder: 'Select Visibility',
            required: true,
          }}
          onChange={val => handleSavePartsFormValues('visibility', val, index)}
        />
        <Field
          name="isDamagedTextual"
          id="isDamagedTextual"
          label="Is it Damaged?"
          component={RadioGroup}
          style={{ marginTop: '10px' }}
          onChange={(e, val) =>
            handleSavePartsFormValues('isDamagedTextual', val, index)
          }
        >
          <FormControlLabel value="yes" control={<Radio />} label="Yes" />
          <FormControlLabel value="no" control={<Radio />} label="No" />
        </Field>
        <div className="footer">
          {loading ? (
            <Button fullWidth buttonclasses={{}}>
              <img
                alt="loader"
                style={{ width: '30px' }}
                src={`${ASSET_URL}/loader_white.gif`}
              />
            </Button>
          ) : (
            <Button fullWidth type="submit" buttonclasses={{}}>
              SAVE
            </Button>
          )}
        </div>
      </form>
    </Wrapper>
  );
}

MotorPartsVisibilityForm.propTypes = {
  handleSavePartsFormValues: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default reduxForm({
  enableReinitialize: true,
  // form: 'motorPartsVisibilityForm',
  // validate,
  onSubmit: submit,
})(MotorPartsVisibilityForm);
