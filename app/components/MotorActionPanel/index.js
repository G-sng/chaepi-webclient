/**
 *
 * MotorActionPanel
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

// import ExpansionPanel from '@material-ui/core/ExpansionPanel';
// import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
// import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import DirectionsCarIcon from '@material-ui/icons/DirectionsCar';
// import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CompareIcon from '@material-ui/icons/Compare';
import BrokenImageIcon from '@material-ui/icons/BrokenImage';

import MotorDamagesSection from './MotorDamagesSection';
import MotorProfileForm from './MotorProfileForm';
import MotorPartsSection from './MotorPartsSection';

import messages from './messages';

const Wrapper = styled.div`
  // flex-grow: 1;
  // width: 100%;
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow: auto;
  .tabs-section {
    min-height: 90px;
  }
  .tab-root {
    min-width: 72px;
  }
  .action-detail-section {
    flex: 1;
    overflow: auto;
  }
`;

const Header = styled.div`
  // display: flex;
  line-height: 40px;
  text-align: center;
  // flex-flow: row nowrap;
  // justify-content: space-between;
  border-bottom: 1px solid #11254b;
  h4 {
    margin: 0px;
    font-weight: 400;
  }
`;

function MotorActionPanel({
  activeTabValue,
  caseNumber,
  handleFetchMotorDamagesSuggestions,
  handleRemoveMotorDamagesSuggestion,
  handleFetchMotorMakeSuggestions,
  handleFetchMotorModelSuggestions,
  handleRemoveMotorPartsSuggestion,
  handleSaveDamagesFormValues,
  handleSaveFormValues,
  handleSavePartsFormValues,
  handleSetActiveTabValue,
  handleSubmitDamage,
  handleSubmitImage,
  handleSubmitPart,
  handleSubmitProfile,
  handleSubmitRejectionReason,
  motorDamagesSuggestions,
  motorMakeSuggestions,
  motorModelSuggestions,
  motorPartsSuggestions,
  motorProfilesSuggestions,
  motorProfileForm,
  // motorPartsSection,
  selectedImage,
}) {
  // const [tabValue, setTabValue] = useState(0);
  // const [expandedPanel, setExpandedPanel] = useState(false);

  return (
    <Wrapper>
      <Header>
        <h4>Case No: {caseNumber}</h4>
      </Header>
      <Tabs
        className="tabs-section"
        value={activeTabValue}
        onChange={(e, val) => handleSetActiveTabValue(val)}
        variant="fullWidth"
        // variant="scrollable"
        // scrollButtons="auto"
        indicatorColor="primary"
        textColor="primary"
      >
        <Tab
          icon={<DirectionsCarIcon />}
          label={<FormattedMessage {...messages.profile} />}
          // label="PROFILE"
          classes={{
            root: 'tab-root',
          }}
        />
        <Tab
          icon={<CompareIcon />}
          label={<FormattedMessage {...messages.parts} />}
          // label="PARTS"
          classes={{
            root: 'tab-root',
          }}
        />
        <Tab
          icon={<BrokenImageIcon />}
          label={<FormattedMessage {...messages.damages} />}
          // label="DAMAGES"
          classes={{
            root: 'tab-root',
          }}
        />
      </Tabs>
      <div className="action-detail-section">
        {activeTabValue === 0 && (
          <MotorProfileForm
            handleFetchMotorMakeSuggestions={handleFetchMotorMakeSuggestions}
            handleFetchMotorModelSuggestions={handleFetchMotorModelSuggestions}
            handleSaveFormValues={handleSaveFormValues}
            handleSubmitProfile={handleSubmitProfile}
            handleSubmitRejectionReason={handleSubmitRejectionReason}
            initialValues={motorProfileForm}
            loading={false}
            motorMakeSuggestions={motorMakeSuggestions}
            motorModelSuggestions={motorModelSuggestions}
            motorProfilesSuggestions={motorProfilesSuggestions}
            // selectedImage={selectedImage}
          />
        )}
        {activeTabValue === 1 && (
          <MotorPartsSection
            // initialValues={motorPartsSection}
            // loading={false}
            handleRemoveMotorPartsSuggestion={handleRemoveMotorPartsSuggestion}
            handleSavePartsFormValues={handleSavePartsFormValues}
            handleSetActiveTabValue={handleSetActiveTabValue}
            handleSubmitPart={handleSubmitPart}
            motorPartsSuggestions={motorPartsSuggestions}
            selectedImage={selectedImage}
          />
        )}
        {activeTabValue === 2 && (
          <MotorDamagesSection
            // initialValues={motorPartsSection}
            // loading={false}
            handleFetchMotorDamagesSuggestions={
              handleFetchMotorDamagesSuggestions
            }
            motorDamagesSuggestions={motorDamagesSuggestions}
            handleRemoveMotorDamagesSuggestion={
              handleRemoveMotorDamagesSuggestion
            }
            handleSaveDamagesFormValues={handleSaveDamagesFormValues}
            handleSubmitImage={handleSubmitImage}
            handleSubmitDamage={handleSubmitDamage}
            selectedImage={selectedImage}
          />
        )}
      </div>
    </Wrapper>
  );
}

MotorActionPanel.propTypes = {
  activeTabValue: PropTypes.number.isRequired,
  caseNumber: PropTypes.string,
  handleFetchMotorDamagesSuggestions: PropTypes.func.isRequired,
  handleRemoveMotorDamagesSuggestion: PropTypes.func.isRequired,
  handleFetchMotorMakeSuggestions: PropTypes.func.isRequired,
  handleFetchMotorModelSuggestions: PropTypes.func.isRequired,
  handleRemoveMotorPartsSuggestion: PropTypes.func.isRequired,
  handleSaveDamagesFormValues: PropTypes.func.isRequired,
  handleSaveFormValues: PropTypes.func.isRequired,
  handleSavePartsFormValues: PropTypes.func.isRequired,
  handleSetActiveTabValue: PropTypes.func.isRequired,
  handleSubmitDamage: PropTypes.func.isRequired,
  handleSubmitImage: PropTypes.func.isRequired,
  handleSubmitPart: PropTypes.func.isRequired,
  handleSubmitProfile: PropTypes.func.isRequired,
  handleSubmitRejectionReason: PropTypes.func.isRequired,
  motorDamagesSuggestions: PropTypes.array.isRequired,
  motorMakeSuggestions: PropTypes.array.isRequired,
  motorModelSuggestions: PropTypes.array.isRequired,
  motorPartsSuggestions: PropTypes.array.isRequired,
  motorProfilesSuggestions: PropTypes.array.isRequired,
  motorPartsSection: PropTypes.object.isRequired,
  motorProfileForm: PropTypes.object.isRequired,
  selectedImage: PropTypes.object.isRequired,
};

export default MotorActionPanel;
