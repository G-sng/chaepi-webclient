function submit(values, dispatch, props) {
  const { handleSubmitProfile } = props;

  if (values.profile && values.visibility) {
    const marking = {
      make: values.make || {}, // as they are optional
      model: values.model || {}, // as they are optional
      profile: values.profile,
      visibility: values.visibility,
    };
    handleSubmitProfile(marking);
  }
}
export default submit;
