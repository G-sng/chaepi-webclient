/**
 *
 * MotorDamagesVisibilityForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Field, reduxForm } from 'redux-form';

import Autocomplete from 'components/MaterialUIFields/Autocomplete';
import Button from 'components/MaterialUIFields/Button';

import {
  ASSET_URL,
  motorDamageActionsSuggestions,
  motorDamageSeveritySuggestions,
} from 'utils/helper';
import { toTitleCase } from 'utils/helperFunctions';

// import validate from './validateProfile';
import submit from './submitDamage';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

const Wrapper = styled.div`
  margin: -15px 10px 10px;
  .input-field {
    margin: 12px 0px;
  }
  .footer {
    margin: 10px 0px;
    margin-bottom: 10px;
  }
`;

function MotorDamagesVisibilityForm({
  damageClasses,
  handleSaveDamagesFormValues,
  handleSubmit,
  index,
  loading,
}) {
  function handleDamageTypeOptions() {
    return damageClasses.map(item => ({
      label: toTitleCase(item.class),
      value: item.class,
    }));
  }

  return (
    <Wrapper>
      <form onSubmit={handleSubmit}>
        <Field
          name="damageType"
          id="damageType"
          component={Autocomplete}
          suggestions={handleDamageTypeOptions()}
          TextFieldProps={{
            label: 'Damage Type',
            InputLabelProps: {
              shrink: true,
            },
            placeholder: 'Select Damage Type',
            required: true,
          }}
          onChange={val =>
            handleSaveDamagesFormValues('damageType', val, index)
          }
        />
        <Field
          name="severity"
          id="severity"
          component={Autocomplete}
          suggestions={motorDamageSeveritySuggestions}
          TextFieldProps={{
            label: 'Severity',
            InputLabelProps: {
              shrink: true,
            },
            placeholder: 'Select Severity',
            required: true,
          }}
          onChange={val => handleSaveDamagesFormValues('severity', val, index)}
        />
        <Field
          name="action"
          id="action"
          component={Autocomplete}
          suggestions={motorDamageActionsSuggestions}
          TextFieldProps={{
            label: 'Action',
            InputLabelProps: {
              shrink: true,
            },
            placeholder: 'Select Action',
            required: true,
          }}
          onChange={val => handleSaveDamagesFormValues('action', val, index)}
        />
        <div className="footer">
          {loading ? (
            <Button fullWidth buttonclasses={{}}>
              <img
                alt="loader"
                style={{ width: '30px' }}
                src={`${ASSET_URL}/loader_white.gif`}
              />
            </Button>
          ) : (
            <Button fullWidth type="submit" buttonclasses={{}}>
              SAVE
            </Button>
          )}
        </div>
      </form>
    </Wrapper>
  );
}

MotorDamagesVisibilityForm.propTypes = {
  damageClasses: PropTypes.array.isRequired,
  handleSaveDamagesFormValues: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default reduxForm({
  enableReinitialize: true,
  // form: 'motorDamagesVisibilityForm',
  // validate,
  onSubmit: submit,
})(MotorDamagesVisibilityForm);
