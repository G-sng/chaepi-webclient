let boundingBoxes = [];
let perimeter = [];
let complete = false;
let canvasRef = null;
let ctx;

function lineIntersects(p0, p1, p2, p3) {
  const s1X = p1.x - p0.x;
  const s1Y = p1.y - p0.y;
  const s2X = p3.x - p2.x;
  const s2Y = p3.y - p2.y;

  const s =
    (-s1Y * (p0.x - p2.x) + s1X * (p0.y - p2.y)) / (-s2X * s1Y + s1X * s2Y);
  const t =
    (s2X * (p0.y - p2.y) - s2Y * (p0.x - p2.x)) / (-s2X * s1Y + s1X * s2Y);

  if (s >= 0 && s <= 1 && t >= 0 && t <= 1) {
    // Collision detected
    return true;
  }
  return false; // No collision
}

function point(x, y) {
  ctx.fillStyle = 'white';
  ctx.strokeStyle = 'white';
  ctx.fillRect(x - 2, y - 2, 4, 4);
  ctx.moveTo(x, y);
}

function drawLine(end) {
  ctx.lineWidth = 1;
  ctx.strokeStyle = 'white';
  ctx.lineCap = 'square';
  ctx.beginPath();

  for (let i = 0; i < perimeter.length; i += 1) {
    if (i === 0) {
      ctx.moveTo(perimeter[i].x, perimeter[i].y);
      // end || point(perimeter[i].x, perimeter[i].y);
      point(perimeter[i].x, perimeter[i].y);
    } else {
      ctx.lineTo(perimeter[i].x, perimeter[i].y);
      // end || point(perimeter[i].x, perimeter[i].y);
      point(perimeter[i].x, perimeter[i].y);
    }
  }
  if (end) {
    ctx.lineTo(perimeter[0].x, perimeter[0].y);
    ctx.closePath();
    // ctx.fillStyle = 'rgba(255, 0, 0, 0.5)';
    // ctx.fill();
    ctx.strokeStyle = 'blue';
    complete = true;
    // reset perimeter and add polygon to bounding box
    boundingBoxes.push(perimeter);
    perimeter = [];
    complete = false;
    console.log(boundingBoxes);
  }
  ctx.stroke();

  // // print coordinates
  // if (perimeter.length === 0) {
  //   document.getElementById('coordinates').value = '';
  // } else {
  //   document.getElementById('coordinates').value = JSON.stringify(perimeter);
  // }
}

function checkIntersect(x, y) {
  if (perimeter.length < 4) {
    return false;
  }
  const p0 = [];
  const p1 = [];
  const p2 = [];
  const p3 = [];

  p2.x = perimeter[perimeter.length - 1].x;
  p2.y = perimeter[perimeter.length - 1].y;
  p3.x = x;
  p3.y = y;

  for (let i = 0; i < perimeter.length - 1; i += 1) {
    p0.x = perimeter[i].x;
    p0.y = perimeter[i].y;
    p1.x = perimeter[i + 1].x;
    p1.y = perimeter[i + 1].y;
    if (p1.x === p2.x && p1.y === p2.y) {
      continue;
    }
    if (p0.x === p3.x && p0.y === p3.y) {
      continue;
    }
    if (lineIntersects(p0, p1, p2, p3) === true) {
      return true;
    }
  }
  return false;
}

export function markPoint(event) {
  if (complete) {
    alert('Polygon already created nigga!');
    return false;
  }
  let x;
  let y;

  if (event.ctrlKey || event.which === 3 || event.button === 2) {
    if (perimeter.length < 3) {
      alert('You need at least three points for a polygon nigga!');
      return false;
    }
    x = perimeter[0].x;
    y = perimeter[0].y;
    if (checkIntersect(x, y)) {
      alert('The line you are drawing intersects another line nigga!');
      return false;
    }
    drawLine(true);
    // alert('Polygon closed');
    event.preventDefault();
    return false;
  }
  const rect = canvasRef.current.getBoundingClientRect();
  x = event.clientX - rect.left;
  y = event.clientY - rect.top;
  if (
    perimeter.length > 0 &&
    x === perimeter[perimeter.length - 1].x &&
    y === perimeter[perimeter.length - 1].y
  ) {
    // same point - double click
    return false;
  }
  if (checkIntersect(x, y)) {
    alert('The line you are drawing intersects another line nigga!');
    return false;
  }
  perimeter.push({ x, y });
  drawLine(false);
  return false;
}

function startPolygon(withDraw) {
  ctx = canvasRef.current.getContext('2d');
  if (withDraw === true) {
    drawLine(false);
  }
}

export function undoPoint() {
  ctx = undefined;
  perimeter.pop();
  complete = false;
  startPolygon(true);
}

export function clearPolygon() {
  ctx = undefined;
  perimeter = [];
  complete = false;
  document.getElementById('coordinates').value = '';
  startPolygon();
}

export function drawPolygon(refCanvas) {
  canvasRef = refCanvas;
  startPolygon();
}

export function initialize() {
  boundingBoxes = [];
  perimeter = [];
  complete = false;
  canvasRef = null;
  ctx = null;
}
