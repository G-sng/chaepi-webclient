import { toTitleCase } from 'utils/helperFunctions';

export const BASE_URL = 'https://chaepi-dev.roadzen.xyz/api/v1';
export const ASSET_URL =
  'https://roadzen-public.s3.ap-south-1.amazonaws.com/dre';

export const motorDamageActionsSuggestions = ['repair', 'replace'].map(
  suggestion => ({
    label: toTitleCase(suggestion),
    value: suggestion,
  }),
);

export const motorDamageSeveritySuggestions = ['high', 'low'].map(
  suggestion => ({
    label: toTitleCase(suggestion),
    value: suggestion,
  }),
);

export const motorRejectionReasonSuggestions = [
  'Heavily Damaged',
  'Blurry / Fuzzy Photo',
  'Too Dark',
  'Too Bright',
  'Irrelevant Photo',
  'Others',
];

export const motorVisibilitySuggestions = [
  'Barely Visible',
  'Around 25%',
  'Around 50%',
  'Around 75%',
  'Fully Visible',
].map(suggestion => ({
  label: suggestion,
  value: suggestion,
}));

export const roles = ['annotator', 'reviewer', 'admin'];
