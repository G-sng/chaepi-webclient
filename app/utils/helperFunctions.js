import moment from 'moment';

export const toTitleCase = str => {
  if (str) {
    return str
      .replace(/_/g, ' ')
      .replace(
        /\w\S*/g,
        txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(),
      );
  }
  return str;
};

export const utcToLocal = utcString => moment.utc(utcString).format('DD/MM/YYYY');
