const dreColors = {
  whiteText: '#ffffff',
  orangeHeading: '#ff6b05',
  blueButton: '#3f51b5',
  blueButtonHover: '#323844',
  yellowCheckbox: '#ffc837',
  blueBlackButton: '#323844',
  yellowHeading: '#ffcf3f',
  dotFinish: '#ffcf3f',
  dotUnfinish: '#cccccc',
  dotComplete: '#8cc63f',
  selectedDropdown: '#ffcf3f',
  tabUnderline: '#ffcf3f',
  yellowText: '#ffcf3f',
  grayButton: '#e6e6e6',
  grayBackground: '#f2f2f2',
  yellowTag: '#ffd221',
  dropDownIcon: '#696969',
  greenState: '#8cc63f',
  redState: '#ff0000',
};

export default dreColors;
