/*
 * ProjectListPage Messages
 *
 * This contains all the text for the ProjectListPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ProjectListPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'PROJECTS',
  },
  recentProjects: {
    id: `${scope}.recentProjects`,
    defaultMessage: 'Recent Projects',
  },
  allProjects: {
    id: `${scope}.allProjects`,
    defaultMessage: 'All Projects',
  },
  motor: {
    id: `${scope}.motor`,
    defaultMessage: 'MOTOR',
  },
  mobile: {
    id: `${scope}.mobile`,
    defaultMessage: 'MOBILE',
  },
});
