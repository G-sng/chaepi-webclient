import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the projectListPage state domain
 */

const selectProjectListPageDomain = state =>
  state.projectListPage || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by ProjectListPage
 */

const makeSelectProjectListPage = () =>
  createSelector(
    selectProjectListPageDomain,
    substate => substate,
  );

export default makeSelectProjectListPage;
export { selectProjectListPageDomain };
