/**
 *
 * ProjectListPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';

import AddIcon from '@material-ui/icons/Add';

import PageDetailsWrapper from 'components/PageDetailsWrapper';
import MasterTable from 'components/MasterTable';

import Page from 'containers/Page';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectProjectListPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

const Wrapper = styled.div``;

const Header = styled.div`
  display: flex;
  line-height: 48px;
  flex-flow: row nowrap;
  justify-content: space-between;
  border-bottom: 1px solid #11254b;
  h4 {
    margin: 0px;
    font-weight: 400;
  }
`;

const ProjectTiles = styled.div`
  display: flex;
  margin: 20px 0px 15px;
  .project-tile {
    padding: 40px;
    margin-right: 20px;
    cursor: pointer;
    &:hover {
      box-shadow: 0 4px 10px 1px rgba(0, 0, 0, 0.3);
      transform: scale(1.01);
    }
  }
`;

export function ProjectListPage({ history }) {
  useInjectReducer({ key: 'projectListPage', reducer });
  useInjectSaga({ key: 'projectListPage', saga });

  const projectListColumns = [
    {
      sort: 0,
      type: 'projectName',
      name: 'PROJECT NAME',
      size: 1,
    },
    {
      sort: 1,
      type: 'projectLead',
      name: 'PROJECT LEAD',
      size: 1,
    },
  ];

  const dummyListData = [
    {
      name: 'motor',
      lead: 'Snoop Dogg',
    },
    {
      name: 'mobile',
      lead: 'Dr DRE',
    },
    {
      name: 'motor',
      lead: 'Snoop Dogg',
    },
    {
      name: 'mobile',
      lead: 'Dr DRE',
    },
    {
      name: 'motor',
      lead: 'Snoop Dogg',
    },
    {
      name: 'mobile',
      lead: 'Dr DRE',
    },
    {
      name: 'motor',
      lead: 'Snoop Dogg',
    },
    {
      name: 'mobile',
      lead: 'Dr DRE',
    },
    {
      name: 'motor',
      lead: 'Snoop Dogg',
    },
  ];

  function handleRenderCell(type, rowData) {
    const cellData = { ...rowData };
    if (type === 'projectName') {
      return <div>{cellData.name}</div>;
    }
    if (type === 'projectLead') {
      return <div>{cellData.lead}</div>;
    }

    return <div>NO CELL TYPE MATCHED!</div>;
  }

  function handleProjectClick(name) {
    history.push(`/${name}`);
  }

  return (
    <Page>
      <Helmet>
        <title>ProjectListPage</title>
        <meta name="description" content="Description of ProjectListPage" />
      </Helmet>
      <PageDetailsWrapper>
        <Wrapper>
          <Header>
            <h4>
              <FormattedMessage {...messages.header} />
            </h4>
            <IconButton aria-label="Add">
              <AddIcon />
            </IconButton>
          </Header>
          <h4 style={{ fontWeight: '400' }}>
            <FormattedMessage {...messages.recentProjects} />
          </h4>
          <ProjectTiles>
            <Paper
              className="project-tile"
              onClick={() => handleProjectClick('motor')}
            >
              <FormattedMessage {...messages.motor} />
            </Paper>
            <Paper
              className="project-tile"
              onClick={() => handleProjectClick('mobile')}
            >
              <FormattedMessage {...messages.mobile} />
            </Paper>
          </ProjectTiles>
          <h4 style={{ fontWeight: '400' }}>
            <FormattedMessage {...messages.allProjects} />
          </h4>
          <MasterTable
            data={dummyListData}
            columns={projectListColumns}
            handleRenderCell={handleRenderCell}
          />
        </Wrapper>
      </PageDetailsWrapper>
    </Page>
  );
}

ProjectListPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  projectListPage: makeSelectProjectListPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(ProjectListPage);
