/**
 *
 * Topbar
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Toolbar from '@material-ui/core/Toolbar';

import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuIcon from '@material-ui/icons/Menu';

import { toggleSidebar } from 'containers/Sidebar/actions';
import { logout } from 'containers/Auth/actions';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectTopbar from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export function Topbar({ dispatch, history }) {
  useInjectReducer({ key: 'topbar', reducer });
  useInjectSaga({ key: 'topbar', saga });

  const [anchorEl, setAnchorEl] = React.useState(null);
  const openUserMenu = !!anchorEl;

  const handleOpenUserMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorEl(null);
  };

  return (
    <AppBar
      // position="fixed"
      style={{
        // zIndex: 1301,
        minHeight: '58px',
        position: 'relative',
      }}
    >
      <Toolbar style={{ minHeight: '58px' }}>
        <IconButton
          color="inherit"
          aria-label="Open sidebar"
          onClick={() => dispatch(toggleSidebar())}
          edge="start"
        >
          <MenuIcon />
        </IconButton>
        <h3 style={{ paddingLeft: '20px', flex: 1 }}>
          <FormattedMessage {...messages.header} />
        </h3>
        {/* <UserMenu /> */}
        <div>
          <IconButton
            aria-label="Account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleOpenUserMenu}
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            keepMounted
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            open={openUserMenu}
            onClose={handleCloseUserMenu}
          >
            <MenuItem onClick={() => dispatch(logout(history))}>
              <FormattedMessage {...messages.logout} />
            </MenuItem>
            {/* <MenuItem onClick={handleCloseUserMenu}>My account</MenuItem> */}
          </Menu>
        </div>
      </Toolbar>
    </AppBar>
  );
}

Topbar.propTypes = {
  dispatch: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  topbar: makeSelectTopbar(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withRouter,
  withConnect,
  memo,
)(Topbar);
