/*
 *
 * Sidebar constants
 *
 */

export const DEFAULT_ACTION = 'app/Sidebar/DEFAULT_ACTION';
export const TOGGLE_SIDEBAR = 'app/Sidebar/TOGGLE_SIDEBAR';
