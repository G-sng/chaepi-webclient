/*
 *
 * Sidebar reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION, TOGGLE_SIDEBAR } from './constants';

export const initialState = {
  showSidebar: false,
};

/* eslint-disable default-case, no-param-reassign */
const sidebarReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case TOGGLE_SIDEBAR:
        draft.showSidebar = !draft.showSidebar;
        break;
    }
  });

export default sidebarReducer;
