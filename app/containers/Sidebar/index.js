/**
 *
 * Sidebar
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import Assessment from '@material-ui/icons/Assessment';
import Folder from '@material-ui/icons/Folder';
import PersonAdd from '@material-ui/icons/PersonAdd';
import Settings from '@material-ui/icons/Settings';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectSidebar from './selectors';
import reducer from './reducer';
import saga from './saga';
// import messages from './messages';
import { toggleSidebar } from './actions';

const Wrapper = styled.div`
  width: 100%;
  min-width: 250px;
  max-width: 350px;
  .sidebar-list {
    padding-top: 80px;
  }
`;

export function Sidebar({ dispatch, history, sidebar }) {
  useInjectReducer({ key: 'sidebar', reducer });
  useInjectSaga({ key: 'sidebar', saga });

  const { showSidebar } = sidebar;

  function handleChangeRoute(route) {
    dispatch(toggleSidebar()); // close sidebar when route change is done
    history.push(`/${route}`);
  }

  return (
    <Drawer
      open={showSidebar}
      onClose={() => dispatch(toggleSidebar())}
      style={{ zIndex: 1099 }} // zIndex is 1 less than that of topbar
    >
      <Wrapper>
        <List className="sidebar-list" component="nav">
          <ListItem button>
            <ListItemIcon>
              <Assessment />
            </ListItemIcon>
            <ListItemText primary="Dashboard" />
          </ListItem>
          <ListItem button onClick={() => handleChangeRoute('project')}>
            <ListItemIcon>
              <Folder />
            </ListItemIcon>
            <ListItemText primary="Project" />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <Settings />
            </ListItemIcon>
            <ListItemText primary="Settings" />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <PersonAdd />
            </ListItemIcon>
            <ListItemText primary="Add User" />
          </ListItem>
        </List>
      </Wrapper>
    </Drawer>
  );
}

Sidebar.propTypes = {
  dispatch: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  sidebar: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  sidebar: makeSelectSidebar(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withRouter,
  withConnect,
)(Sidebar);
