/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';

import AuthRoute from 'components/AuthRoute';
import Auth from 'containers/Auth';
import HomePage from 'containers/HomePage/Loadable';
import LoginPage from 'containers/LoginPage/Loadable';
import MotorDetailsPage from 'containers/MotorDetailsPage/Loadable';
import MotorListPage from 'containers/MotorListPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import ProjectListPage from 'containers/ProjectListPage/Loadable';

import GlobalStyle from '../../global-styles';

export default function App() {
  return (
    <>
      <Auth
        renderRoutes={isAuthenticated => (
          <Switch>
            <Route exact path="/login" component={LoginPage} />
            <AuthRoute
              isAuthenticated={isAuthenticated}
              exact
              path="/"
              component={HomePage}
            />
            <AuthRoute
              isAuthenticated={isAuthenticated}
              exact
              path="/project"
              component={ProjectListPage}
            />
            <AuthRoute
              isAuthenticated={isAuthenticated}
              exact
              path="/motor"
              component={MotorListPage}
            />
            <AuthRoute
              isAuthenticated={isAuthenticated}
              exact
              path="/motor/:id"
              component={MotorDetailsPage}
            />
            {/* <AuthRoute isAuthenticated={isAuthenticated} exact path="/cases" component={CaseListPage} /> No route access check */}
            <Route component={NotFoundPage} />
          </Switch>
        )}
      />
      <GlobalStyle />
    </>
  );
}
