/*
 *
 * LoginPage reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION, JWT_LOGIN } from './constants';

export const initialState = {
  loading: false,
  errorMessage: '',
  logoImage: '',
  user: {},
  optGenerated: false,
};

/* eslint-disable default-case, no-param-reassign */
const loginPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case JWT_LOGIN:
        draft.loading = true;
        break;
      case `${JWT_LOGIN}_SUCCESS`:
        draft.loading = false;
        draft.errorMessage = '';
        break;
      case `${JWT_LOGIN}_FAILURE`:
        draft.loading = false;
        draft.errorMessage = action.error.message;
        break;
    }
  });

export default loginPageReducer;
