/**
 *
 * LoginPage
 *
 */

import React, { memo, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Helmet } from 'react-helmet';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import styled from 'styled-components';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

import PasswordLoginForm from 'components/PasswordLoginForm';
import Page from 'containers/Page';
import { ASSET_URL } from 'utils/helper';
import dreColors from 'themes/dre';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { makeSelectLoginPage, makeSelectAuth } from './selectors';
import reducer from './reducer';
import saga from './saga';
// import messages from './messages';
import { jwtLogin } from './actions';

const Wrapper = styled.section`
  display: flex;
  flex: 1;
  background: url(${ASSET_URL}/dre-ai.jpg);
  background-size: cover !important;
  align-items: center;
  justify-content: center;
`;

export function LoginPage({ auth, location, loginPage, dispatch }) {
  useInjectReducer({ key: 'loginPage', reducer });
  useInjectSaga({ key: 'loginPage', saga });

  const [redirectToReferrer] = useState(false);

  const { loading, errorMessage } = loginPage;

  const { from } = location.state || { from: { pathname: '/' } };
  if (redirectToReferrer || auth.isAuthenticated) {
    return <Redirect to={from} />;
  }

  return (
    <Page hideBars>
      <Wrapper>
        <Helmet>
          <title>LoginPage</title>
          <meta name="description" content="Description of LoginPage" />
        </Helmet>
        {/* <FormattedMessage {...messages.header} /> */}
        <Card
          style={{
            width: '400px',
            padding: '30px 10px 0px',
            borderRadius: '10px',
            backgroundColor: 'rgba(255, 255, 255, .8)',
            // backgroundColor: 'rgba(90, 160, 160, .85)',
            boxShadow: '0 1px 10px rgba(0, 0, 0, .1)',
          }}
        >
          <CardContent>
            {errorMessage && (
              <div
                style={{
                  position: 'absolute',
                  left: '50%',
                  transform: 'translateX(-50%)',
                  fontSize: '16px',
                  color: dreColors.orangeHeading,
                }}
              >
                {errorMessage}
              </div>
            )}
            <PasswordLoginForm
              onSubmit={values => dispatch(jwtLogin(values))}
              loading={loading}
            />
          </CardContent>
          <CardActions>
            {/* <Button size="small">Learn More</Button> */}
          </CardActions>
        </Card>
      </Wrapper>
    </Page>
  );
}

LoginPage.propTypes = {
  auth: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
  loginPage: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  loginPage: makeSelectLoginPage(),
  auth: makeSelectAuth(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(LoginPage);
