import request from 'superagent';
import { call, put, takeLatest } from 'redux-saga/effects';

import { loginFlow, syncStorageAuthSaga } from 'containers/Auth/saga';
import { BASE_URL } from 'utils/helper';

import { JWT_LOGIN } from './constants';

export function* jwtLoginSaga(action) {
  try {
    const data = yield request
      .post(`${BASE_URL}/auth/login`)
      .send(action.value)
      .then(response => {
        if (!response.ok) {
          throw new Error('Something goes wrong with API');
        }
        return response.body;
      });

    yield call(loginFlow, data); // call is used as we need to wait for localforage to get set, after that we can sync the storage
    yield call(syncStorageAuthSaga); // call is used as we need to wait for localforage to get set, after that we can sync the storage
    yield put({ type: `${JWT_LOGIN}_SUCCESS` });
  } catch (error) {
    yield put({ type: `${JWT_LOGIN}_FAILURE`, error: error.response.body });
  }
}

// Individual exports for testing
export default function* loginPageSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(JWT_LOGIN, jwtLoginSaga);
}
