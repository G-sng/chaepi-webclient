/*
 *
 * LoginPage actions
 *
 */

import { DEFAULT_ACTION, JWT_LOGIN } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function jwtLogin(value) {
  return {
    type: JWT_LOGIN,
    value,
  };
}
