/*
 *
 * LoginPage constants
 *
 */

export const DEFAULT_ACTION = 'app/LoginPage/DEFAULT_ACTION';
export const JWT_LOGIN = 'app/LoginPage/JWT_LOGIN';
