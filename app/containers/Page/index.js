/**
 *
 * Page
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
// import { FormattedMessage } from 'react-intl';
import { compose } from 'redux';

import Topbar from 'containers/Topbar';
import Sidebar from 'containers/Sidebar';

// import messages from './messages';

const Wrapper = styled.div`
  display: flex;
`;

export function Page({ hideBars, children }) {
  return (
    <Wrapper>
      {hideBars ? (
        <>{children}</>
      ) : (
        <>
          <Topbar />
          <Sidebar />
          {children}
        </>
      )}
    </Wrapper>
  );
}

Page.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  // page: PropTypes.any,
  children: PropTypes.node.isRequired,
  hideBars: PropTypes.bool,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(withConnect)(Page);
