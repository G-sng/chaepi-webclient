/**
 *
 * CanvasContainer
 *
 */

import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

// import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';

// import CropSquareIcon from '@material-ui/icons/CropSquare';
import DeleteIcon from '@material-ui/icons/Delete';
import TimelineIcon from '@material-ui/icons/Timeline';
import UndoIcon from '@material-ui/icons/Undo';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectCanvasContainer from './selectors';
import reducer from './reducer';
import saga from './saga';
// import messages from './messages';
import {
  appendCompletedPolygon,
  removeCompletedPolygons,
  setImageDimensions,
} from './actions';

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  min-height: 90%;
  height: 90%;
  max-height: 90%;
  // min-height: 100%;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
  .left-menu {
    flex: 1;
    max-width: 42px;
    background: #fcfcfc;
    border-right: 1px solid rgba(0, 0, 0, 0.12);
    .left-menu-icon {
      padding: 8px;
    }
  }
  .right-canvas {
    overflow: auto;
    padding: 10px;
    flex: 8;
  }
`;

export function CanvasContainer({ dispatch, selectedImage, canvasContainer }) {
  useInjectReducer({ key: 'canvasContainer', reducer });
  useInjectSaga({ key: 'canvasContainer', saga });

  const [selectedShape, setSelectedShape] = React.useState('polygon');
  const canvasRef = useRef(null);

  const {
    completedPolygons,
    // imageHeight,
    // imageWidth,
  } = canvasContainer;

  // const completedPolygons = []; // completed but not sent to backend
  const polygonMarkings = []; // synced from backend
  // const rectangleMarkings = []; // synced from backend
  let activePolygonCompleted = false; // false=incomplete, true=complete
  let incompletePolygon = [];
  let ctx = null;

  useEffect(() => {
    ctx = canvasRef.current.getContext('2d');
  });

  // useEffect(() => {
  //   if (
  //     completedPolygons.length === 0 &&
  //     imageHeight !== null &&
  //     imageWidth !== null
  //   ) {
  //     ctx.clearRect(0, 0, imageHeight, imageWidth);
  //     handleParseMarkings(selectedImage.markings);
  //     drawInitialPolygons();
  //     startPolygon();
  //   }
  // }, [completedPolygons]);

  useEffect(() => {
    ctx = canvasRef.current.getContext('2d');
    const canvasTag = canvasRef.current;
    canvasTag.addEventListener('mousemove', drawTempLine, false);

    function drawTempLine(event) {
      if (completedPolygons.length === 0) {
        // linear gradient from start to end of line
        // Create gradient
        const grad = ctx.createLinearGradient(
          0,
          canvasRef.current.height,
          canvasRef.current.width,
          canvasRef.current.height,
        );
        // Add colors
        grad.addColorStop(0, 'rgba(255, 0, 0, 1.000)');
        grad.addColorStop(0.15, 'rgba(255, 0, 255, 1.000)');
        grad.addColorStop(0.33, 'rgba(0, 0, 255, 1.000)');
        grad.addColorStop(0.49, 'rgba(0, 255, 255, 1.000)');
        grad.addColorStop(0.67, 'rgba(0, 255, 0, 1.000)');
        grad.addColorStop(0.84, 'rgba(255, 255, 0, 1.000)');
        grad.addColorStop(1, 'rgba(255, 0, 0, 1.000)');

        const rect = canvasRef.current.getBoundingClientRect();
        const x = event.clientX - rect.left;
        const y = event.clientY - rect.top;
        const canvasImg = new Image();
        canvasImg.onload = () => {
          // ctx.drawImage(canvasImg, 0, 0, canvasImg.naturalWidth, canvasImg.naturalHeight, 0, 0, imgWidth, imgHeight);
          canvasRef.current.height = canvasImg.naturalHeight;
          canvasRef.current.width = canvasImg.naturalWidth;
          ctx.drawImage(canvasImg, 0, 0);
          handleParseMarkings(selectedImage.markings);
          drawInitialPolygons();
          startPolygon();
          drawIncompletePolyLine([...incompletePolygon, { x, y }], grad);
        };
        if (selectedImage.signedUrl) {
          canvasImg.src = selectedImage.signedUrl;
        }
      }
    }

    return () => {
      canvasTag.removeEventListener('mousemove', drawTempLine, false);
    };
  }, [canvasRef, incompletePolygon]);

  useEffect(() => {
    // console.log('uselayouteffect------>  ');
    ctx = canvasRef.current.getContext('2d');
    const canvasImg = new Image();
    canvasImg.onload = () => {
      // ctx.drawImage(canvasImg, 0, 0, canvasImg.naturalWidth, canvasImg.naturalHeight, 0, 0, imgWidth, imgHeight);
      canvasRef.current.height = canvasImg.naturalHeight;
      canvasRef.current.width = canvasImg.naturalWidth;
      ctx.drawImage(canvasImg, 0, 0);
      handleParseMarkings(selectedImage.markings);
      drawInitialPolygons();
      startPolygon();
      dispatch(
        setImageDimensions(canvasImg.naturalHeight, canvasImg.naturalWidth),
      );
    };
    if (selectedImage.signedUrl) {
      canvasImg.src = selectedImage.signedUrl;
    }
    // return () => {
    //   ctx.clearRect(0, 0, canvasImg.naturalHeight, canvasImg.naturalHeight);
    // };
  }, [selectedImage]);

  function handleParseMarkings(markings) {
    if (markings) {
      markings.map(item => {
        if (item.boundingBox && item.boundingBox.polygons.length > 0) {
          // marking is a polygon
          polygonMarkings.push(item.boundingBox.polygons[0]); // as of now, 1 item will have 1 bounding box
        } else if (item.boundingBox && item.boundingBox.rectangles.length > 0) {
          // polygonMarkings.push(item.boundingBox.rectangles[0]); // as of now, 1 item will have 1 bounding box
        }
        return item;
      });
    }
    return true;
  }

  function drawInitialPolygons() {
    polygonMarkings.map(item => {
      drawPolyLine(item, 'red');
      return item;
    });
  }

  function drawPolyLine(shape, color) {
    ctx.lineWidth = 2;
    ctx.strokeStyle = color;
    ctx.lineCap = 'square';
    ctx.beginPath();
    ctx.imageSmoothingEnabled = true;
    shape.map((cords, key) => {
      if (key === 0) {
        ctx.moveTo(cords.x, cords.y);
      } else {
        ctx.lineTo(cords.x, cords.y);
      }
      point(cords.x, cords.y);
      return cords;
    });
    // finish/close the polygon
    ctx.lineTo(shape[0].x, shape[0].y);
    ctx.closePath();
    ctx.strokeStyle = color;
    ctx.stroke();
  }

  function drawIncompletePolyLine(shape, color) {
    ctx.lineWidth = 2;
    ctx.strokeStyle = color;
    ctx.lineCap = 'square';
    ctx.beginPath();
    ctx.imageSmoothingEnabled = true;
    // ctx.translate(0.5, 0.5);
    shape.map((cords, key) => {
      if (key === 0) {
        ctx.moveTo(cords.x, cords.y);
      } else {
        ctx.lineTo(cords.x, cords.y);
      }
      point(cords.x, cords.y);
      return cords;
    });
    ctx.stroke();
  }

  function startPolygon(withDraw) {
    if (withDraw === true) {
      drawLine(false);
    }
  }

  function drawLine(end) {
    // linear gradient from start to end of line
    // Create gradient
    const grad = ctx.createLinearGradient(
      0,
      canvasRef.current.height,
      canvasRef.current.width,
      canvasRef.current.height,
    );
    // Add colors
    grad.addColorStop(0, 'rgba(255, 0, 0, 1.000)');
    grad.addColorStop(0.15, 'rgba(255, 0, 255, 1.000)');
    grad.addColorStop(0.33, 'rgba(0, 0, 255, 1.000)');
    grad.addColorStop(0.49, 'rgba(0, 255, 255, 1.000)');
    grad.addColorStop(0.67, 'rgba(0, 255, 0, 1.000)');
    grad.addColorStop(0.84, 'rgba(255, 255, 0, 1.000)');
    grad.addColorStop(1, 'rgba(255, 0, 0, 1.000)');

    ctx.strokeStyle = grad;
    ctx.lineWidth = 2;
    // ctx.strokeStyle = 'white';
    ctx.lineCap = 'square';
    ctx.beginPath();
    ctx.imageSmoothingEnabled = true;

    for (let i = 0; i < incompletePolygon.length; i += 1) {
      if (i === 0) {
        ctx.moveTo(incompletePolygon[i].x, incompletePolygon[i].y);
        // end || point(perimeter[i].x, perimeter[i].y);
        point(incompletePolygon[i].x, incompletePolygon[i].y);
      } else {
        ctx.lineTo(incompletePolygon[i].x, incompletePolygon[i].y);
        // end || point(perimeter[i].x, perimeter[i].y);
        point(incompletePolygon[i].x, incompletePolygon[i].y);
      }
    }
    if (end) {
      // reStart();
      ctx.lineTo(incompletePolygon[0].x, incompletePolygon[0].y);
      ctx.closePath();
      // ctx.fillStyle = 'rgba(255, 0, 0, 0.5)';
      // ctx.fill();
      ctx.strokeStyle = 'blue';
      activePolygonCompleted = true;
      // reset perimeter and add polygon to bounding box
      // completedPolygons.push(incompletePolygon);
      dispatch(appendCompletedPolygon(incompletePolygon));
      incompletePolygon = [];
      activePolygonCompleted = false;
    }
    ctx.stroke();
  }

  function point(x, y) {
    ctx.fillStyle = 'white';
    // ctx.strokeStyle = 'white';
    ctx.fillRect(x - 2, y - 2, 4, 4);
    ctx.moveTo(x, y);
  }

  function undoPoint() {
    ctx = null;
    incompletePolygon.pop();
    activePolygonCompleted = false;
    reStart();
  }

  function clearPolygon() {
    ctx = null;
    incompletePolygon = [];
    activePolygonCompleted = false;
    dispatch(removeCompletedPolygons());
    reStart();
  }

  function reStart() {
    ctx = canvasRef.current.getContext('2d');
    const grad = ctx.createLinearGradient(
      0,
      canvasRef.current.height,
      canvasRef.current.width,
      canvasRef.current.height,
    );
    // Add colors
    grad.addColorStop(0, 'rgba(255, 0, 0, 1.000)');
    grad.addColorStop(0.15, 'rgba(255, 0, 255, 1.000)');
    grad.addColorStop(0.33, 'rgba(0, 0, 255, 1.000)');
    grad.addColorStop(0.49, 'rgba(0, 255, 255, 1.000)');
    grad.addColorStop(0.67, 'rgba(0, 255, 0, 1.000)');
    grad.addColorStop(0.84, 'rgba(255, 255, 0, 1.000)');
    grad.addColorStop(1, 'rgba(255, 0, 0, 1.000)');
    const canvasImg = new Image();
    canvasImg.onload = () => {
      // ctx.drawImage(canvasImg, 0, 0, canvasImg.naturalWidth, canvasImg.naturalHeight, 0, 0, imgWidth, imgHeight);
      canvasRef.current.height = canvasImg.naturalHeight;
      canvasRef.current.width = canvasImg.naturalWidth;
      ctx.drawImage(canvasImg, 0, 0);
      handleParseMarkings(selectedImage.markings);
      drawInitialPolygons();
      startPolygon();
      drawIncompletePolyLine(incompletePolygon, grad);
    };
    if (selectedImage.signedUrl) {
      canvasImg.src = selectedImage.signedUrl;
    }
  }

  function markPoint(event) {
    if (completedPolygons.length > 0) {
      alert('Please save/remove already created polygon!');
      return false;
    }
    if (activePolygonCompleted) {
      alert('Polygon already created!');
      return false;
    }
    let x;
    let y;

    if (event.ctrlKey || event.which === 3 || event.button === 2) {
      if (incompletePolygon.length < 3) {
        alert('You need at least three points for a polygon!');
        return false;
      }
      x = incompletePolygon[0].x;
      y = incompletePolygon[0].y;
      if (checkIntersect(x, y)) {
        alert('The line you are drawing intersects another line!');
        return false;
      }
      drawLine(true);
      // alert('Polygon closed');
      event.preventDefault();
      return false;
    }
    const rect = canvasRef.current.getBoundingClientRect();
    x = event.clientX - rect.left;
    y = event.clientY - rect.top;
    if (
      incompletePolygon.length > 0 &&
      x === incompletePolygon[incompletePolygon.length - 1].x &&
      y === incompletePolygon[incompletePolygon.length - 1].y
    ) {
      // same point - double click
      return false;
    }
    if (checkIntersect(x, y)) {
      alert('The line you are drawing intersects another line!');
      return false;
    }
    incompletePolygon.push({ x, y });
    drawLine(false);
    return false;
  }

  function checkIntersect(x, y) {
    if (incompletePolygon.length < 4) {
      return false;
    }
    const p0 = [];
    const p1 = [];
    const p2 = [];
    const p3 = [];

    p2.x = incompletePolygon[incompletePolygon.length - 1].x;
    p2.y = incompletePolygon[incompletePolygon.length - 1].y;
    p3.x = x;
    p3.y = y;

    for (let i = 0; i < incompletePolygon.length - 1; i += 1) {
      p0.x = incompletePolygon[i].x;
      p0.y = incompletePolygon[i].y;
      p1.x = incompletePolygon[i + 1].x;
      p1.y = incompletePolygon[i + 1].y;
      if (p1.x === p2.x && p1.y === p2.y) {
        continue;
      }
      if (p0.x === p3.x && p0.y === p3.y) {
        continue;
      }
      if (lineIntersects(p0, p1, p2, p3) === true) {
        return true;
      }
    }
    return false;
  }

  function lineIntersects(p0, p1, p2, p3) {
    const s1X = p1.x - p0.x;
    const s1Y = p1.y - p0.y;
    const s2X = p3.x - p2.x;
    const s2Y = p3.y - p2.y;

    const s =
      (-s1Y * (p0.x - p2.x) + s1X * (p0.y - p2.y)) / (-s2X * s1Y + s1X * s2Y);
    const t =
      (s2X * (p0.y - p2.y) - s2Y * (p0.x - p2.x)) / (-s2X * s1Y + s1X * s2Y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1) {
      // Collision detected
      return true;
    }
    return false; // No collision
  }

  return (
    <Wrapper>
      <div className="left-menu">
        <List component="nav" aria-label="Main mailbox folders">
          <ListItem
            className="left-menu-icon"
            button
            selected={selectedShape === 'polygon'}
            onClick={() => setSelectedShape('polygon')}
          >
            <ListItemIcon>
              <TimelineIcon />
            </ListItemIcon>
          </ListItem>
          {/* <Divider /> */}
          {/* <ListItem
            button
            selected={selectedShape === 'rectangle'}
            onClick={() => setSelectedShape('rectangle')}
          >
            <ListItemIcon>
              <CropSquareIcon />
            </ListItemIcon>
          </ListItem> */}
          {/* <Divider /> */}
          <ListItem className="left-menu-icon" button onClick={undoPoint}>
            <ListItemIcon>
              <UndoIcon />
            </ListItemIcon>
          </ListItem>
          {/* <Divider /> */}
          <ListItem className="left-menu-icon" button onClick={clearPolygon}>
            <ListItemIcon>
              <DeleteIcon />
            </ListItemIcon>
          </ListItem>
        </List>
      </div>
      <div className="right-canvas">
        <canvas
          ref={canvasRef}
          onMouseDown={markPoint}
          onContextMenu={e => e.preventDefault()}
          style={{
            cursor: 'crosshair',
          }}
        />
      </div>
    </Wrapper>
  );
}

CanvasContainer.propTypes = {
  dispatch: PropTypes.func.isRequired,
  canvasContainer: PropTypes.object.isRequired,
  selectedImage: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  canvasContainer: makeSelectCanvasContainer(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(CanvasContainer);
