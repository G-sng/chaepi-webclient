/*
 * CanvasContainer Messages
 *
 * This contains all the text for the CanvasContainer container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.CanvasContainer';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the CanvasContainer container!',
  },
});
