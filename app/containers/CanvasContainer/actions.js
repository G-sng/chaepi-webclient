/*
 *
 * CanvasContainer actions
 *
 */

import {
  DEFAULT_ACTION,
  APPEND_COMPLETED_POLYGON,
  REMOVE_COMPLETED_POLYGONS,
  SET_IMAGE_DIMENSIONS,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function appendCompletedPolygon(value) {
  return {
    type: APPEND_COMPLETED_POLYGON,
    value,
  };
}

export function removeCompletedPolygons() {
  return {
    type: REMOVE_COMPLETED_POLYGONS,
  };
}

export function setImageDimensions(height, width) {
  return {
    type: SET_IMAGE_DIMENSIONS,
    height,
    width,
  };
}
