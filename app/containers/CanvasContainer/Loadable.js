/**
 *
 * Asynchronously loads the component for CanvasContainer
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
