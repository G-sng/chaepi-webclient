import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the canvasContainer state domain
 */

const selectCanvasContainerDomain = state =>
  state.canvasContainer || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by CanvasContainer
 */

const makeSelectCanvasContainer = () =>
  createSelector(
    selectCanvasContainerDomain,
    substate => substate,
  );

export default makeSelectCanvasContainer;
export { selectCanvasContainerDomain };
