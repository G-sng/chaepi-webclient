/*
 *
 * CanvasContainer constants
 *
 */

export const DEFAULT_ACTION = 'app/CanvasContainer/DEFAULT_ACTION';
export const APPEND_COMPLETED_POLYGON =
  'app/CanvasContainer/APPEND_COMPLETED_POLYGON';
export const SET_IMAGE_DIMENSIONS = 'app/CanvasContainer/SET_IMAGE_DIMENSIONS';
export const REMOVE_COMPLETED_POLYGONS =
  'app/CanvasContainer/REMOVE_COMPLETED_POLYGONS';
