/*
 *
 * CanvasContainer reducer
 *
 */
import produce from 'immer';
import {
  DEFAULT_ACTION,
  APPEND_COMPLETED_POLYGON,
  REMOVE_COMPLETED_POLYGONS,
  SET_IMAGE_DIMENSIONS,
} from './constants';

export const initialState = {
  completedPolygons: [],
  imageHeight: null,
  imageWidth: null,
};

/* eslint-disable default-case, no-param-reassign */
const canvasContainerReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case APPEND_COMPLETED_POLYGON:
        draft.completedPolygons.push(action.value);
        break;
      case SET_IMAGE_DIMENSIONS:
        draft.imageHeight = action.height;
        draft.imageWidth = action.width;
        break;
      case REMOVE_COMPLETED_POLYGONS:
        draft.completedPolygons = [];
        break;
    }
  });

export default canvasContainerReducer;
