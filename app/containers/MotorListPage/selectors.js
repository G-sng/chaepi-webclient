import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the motorListPage state domain
 */

const selectMotorListPageDomain = state => state.motorListPage || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by MotorListPage
 */

const makeSelectMotorListPage = () =>
  createSelector(
    selectMotorListPageDomain,
    substate => substate,
  );

export default makeSelectMotorListPage;
export { selectMotorListPageDomain };
