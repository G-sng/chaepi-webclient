/*
 *
 * MotorListPage actions
 *
 */

import {
  DEFAULT_ACTION,
  CHANGE_CASE_CREATION_DIALOG_STATUS,
  CHANGE_LIST_PAGE_OFFSET,
  FETCH_MOTOR_CASE_LIST,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function changeCaseCreationDialogStatus(value) {
  return {
    type: CHANGE_CASE_CREATION_DIALOG_STATUS,
    value,
  };
}

export function changeListPageOffset(value) {
  return {
    type: CHANGE_LIST_PAGE_OFFSET,
    value,
  };
}

export function fetchMotorCaseList(value) {
  return {
    type: FETCH_MOTOR_CASE_LIST,
    value,
  };
}
