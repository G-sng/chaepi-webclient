/*
 *
 * MotorListPage constants
 *
 */

export const DEFAULT_ACTION = 'app/MotorListPage/DEFAULT_ACTION';
export const CHANGE_CASE_CREATION_DIALOG_STATUS =
  'app/MotorListPage/CHANGE_CASE_CREATION_DIALOG_STATUS';
export const CHANGE_LIST_PAGE_OFFSET =
  'app/MotorListPage/CHANGE_LIST_PAGE_OFFSET';
export const FETCH_MOTOR_CASE_LIST = 'app/MotorListPage/FETCH_MOTOR_CASE_LIST';
