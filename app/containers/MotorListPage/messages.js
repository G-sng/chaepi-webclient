/*
 * MotorListPage Messages
 *
 * This contains all the text for the MotorListPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.MotorListPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'MOTOR',
  },
  caseList: {
    id: `${scope}.caseList`,
    defaultMessage: 'Case List',
  },
});
