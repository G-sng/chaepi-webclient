/*
 *
 * MotorListPage reducer
 *
 */
import produce from 'immer';
import {
  DEFAULT_ACTION,
  CHANGE_CASE_CREATION_DIALOG_STATUS,
  CHANGE_LIST_PAGE_OFFSET,
  FETCH_MOTOR_CASE_LIST,
} from './constants';

export const initialState = {
  showCaseCreationDialog: false,
  cases: [],
  metadata: {
    count: 0,
    page: 0,
    pageSize: 0,
    total: 0,
    totalPages: 0,
  },
};

/* eslint-disable default-case, no-param-reassign */
const motorListPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case CHANGE_CASE_CREATION_DIALOG_STATUS:
        draft.showCaseCreationDialog = action.value;
        break;
      case CHANGE_LIST_PAGE_OFFSET:
        draft.metadata.page = action.value;
        break;
      case `${FETCH_MOTOR_CASE_LIST}_SUCCESS`:
        draft.cases = action.data.cases;
        draft.metadata = action.data.metadata;
        break;
    }
  });

export default motorListPageReducer;
