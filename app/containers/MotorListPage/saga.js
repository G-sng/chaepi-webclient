import request from 'superagent';
import { call, put, takeLatest } from 'redux-saga/effects';
import { getToken } from 'containers/Auth/saga';

import { BASE_URL } from 'utils/helper';

import { FETCH_MOTOR_CASE_LIST } from './constants';

export function* fetchMotorCaseListSaga(action) {
  const token = yield call(getToken);

  try {
    const data = yield request
      .get(`${BASE_URL}/cases?projectId=1&page=${action.value}`)
      .set('Authorization', token)
      .then(response => {
        if (!response.ok) {
          throw new Error('Something Went Wrong');
        }
        return response.body;
      });

    yield put({ type: `${FETCH_MOTOR_CASE_LIST}_SUCCESS`, data });
  } catch (error) {
    console.error('error-----> ', error);
    alert(error.response.body.message);
    // if (error.status === 400) {
    //   console.error(error.response.body.reason);
    // }
    // yield put({ type: `${UPDATE_CASE_DETAILS}_FAILURE`, vehicle });
  }
}

export default function* motorListPageSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(FETCH_MOTOR_CASE_LIST, fetchMotorCaseListSaga);
}
