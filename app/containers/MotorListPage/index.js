/**
 *
 * MotorListPage
 *
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import ReactPaginate from 'react-paginate';

import IconButton from '@material-ui/core/IconButton';
import Dialog from '@material-ui/core/Dialog';

import AddIcon from '@material-ui/icons/Add';

import PageDetailsWrapper from 'components/PageDetailsWrapper';
import MasterTable from 'components/MasterTable';

import MotorCaseCreation from 'containers/MotorCaseCreation';
import Page from 'containers/Page';

import { toTitleCase, utcToLocal } from 'utils/helperFunctions';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectMotorListPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import {
  changeCaseCreationDialogStatus,
  changeListPageOffset,
  fetchMotorCaseList,
} from './actions';

const Wrapper = styled.div``;

const Header = styled.div`
  display: flex;
  line-height: 48px;
  flex-flow: row nowrap;
  justify-content: space-between;
  border-bottom: 1px solid #11254b;
  h4 {
    margin: 0px;
    font-weight: 400;
  }
`;

const PaginatorWrapper = styled.div`
  padding-left: 15px;
  margin-bottom: 20px;
  margin-top: 30px;
  color: #808080;
  max-width: 800px;
  overflow-x: scroll;
  white-space: nowrap;

  .pagination {
    box-shadow: rgba(0, 0, 0, 0.3) 0px 0px 0px, rgba(0, 0, 0, 0.2) 0px 2px 12px;
    border-radius: 4px;
    font-size: 14px;
    line-height: 20px;
    display: inline-block;
    padding-left: 5px;
    padding-right: 5px;
    cursor: pointer;
    li {
      display: inline-block;
      padding: 3px;
      a {
        padding: 6px;
        color: #333;
        background: transparent;
        // border: 1px solid #333;
      }
      span {
        padding: 6px;
        color: #333;
        background: transparent;
        // border: 1px solid #333;
      }
    }
    .active {
      span {
        background: #3f51b5;
        // border-color: #3f51b5;
        // border: 1px solid #3f51b5;
        z-index: 0;
        cursor: pointer;
        color: #fff;
      }
      a {
        background: #3f51b5;
        // border-color: #3f51b5;
        // border: 1px solid #3f51b5;
        z-index: 0;
        cursor: pointer;
        color: #fff;
      }
    }
  }
`;

export function MotorListPage({ dispatch, history, motorListPage }) {
  useInjectReducer({ key: 'motorListPage', reducer });
  useInjectSaga({ key: 'motorListPage', saga });

  const { cases, metadata, showCaseCreationDialog } = motorListPage;

  const projectListColumns = [
    {
      sort: 0,
      type: 'caseId',
      name: 'CASE ID',
      size: 1,
    },
    {
      sort: 1,
      type: 'assignedTo',
      name: 'ASSIGNED TO',
      size: 1,
    },
    {
      sort: 2,
      type: 'caseStatus',
      name: 'CASE STATUS',
      size: 1,
    },
    {
      sort: 3,
      type: 'createdOn',
      name: 'CREATED ON',
      size: 1,
    },
  ];

  useEffect(() => {
    dispatch(fetchMotorCaseList(metadata.page));
  }, [metadata.page]);

  function handleRenderCell(type, rowData) {
    const cellData = { ...rowData };
    if (type === 'caseId') {
      return <div>{cellData.caseNumber}</div>;
    }
    if (type === 'assignedTo') {
      return <div>{cellData.operator.name}</div>;
    }
    if (type === 'caseStatus') {
      return <div>{toTitleCase(cellData.status)}</div>;
    }
    if (type === 'createdOn') {
      return <div>{utcToLocal(cellData.createdAt)}</div>;
    }

    return <div>NO CELL TYPE MATCHED!</div>;
  }

  return (
    <Page>
      <Helmet>
        <title>MotorListPage</title>
        <meta name="description" content="Description of MotorListPage" />
      </Helmet>
      <PageDetailsWrapper>
        <Wrapper>
          <Header>
            <h4>
              <FormattedMessage {...messages.header} />
            </h4>
            <IconButton
              aria-label="Add"
              onClick={() => dispatch(changeCaseCreationDialogStatus(true))}
            >
              <AddIcon />
            </IconButton>
          </Header>
          <h4 style={{ fontWeight: '400' }}>
            <FormattedMessage {...messages.caseList} />
          </h4>
          <MasterTable
            data={cases}
            columns={projectListColumns}
            handleRenderCell={handleRenderCell}
            handleRowItemClick={row => history.push(`/motor/${row.id}`)}
          />
          <PaginatorWrapper>
            <ReactPaginate
              previousLabel="Previous"
              nextLabel="Next"
              breakLabel="..."
              breakClassName="break-me"
              pageCount={metadata.totalPages}
              // marginPagesDisplayed={2}
              // pageRangeDisplayed={5}
              onPageChange={val => dispatch(changeListPageOffset(val.selected))}
              initialPage={metadata.page}
              containerClassName="pagination"
              subContainerClassName="pages pagination"
              activeClassName="active"
            />
          </PaginatorWrapper>
        </Wrapper>
        <Dialog
          open={showCaseCreationDialog}
          fullWidth
          // onClose={() => dispatch(changeCaseCreationDialogStatus(false))}
          aria-labelledby="case-creation"
        >
          <MotorCaseCreation
            handleCaseCreationSuccess={() =>
              dispatch(changeCaseCreationDialogStatus(false)) &&
              dispatch(fetchMotorCaseList(0))
            }
            handleChangeCaseCreationDialogStatus={() =>
              dispatch(changeCaseCreationDialogStatus(false))
            }
          />
        </Dialog>
      </PageDetailsWrapper>
    </Page>
  );
}

MotorListPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  motorListPage: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  motorListPage: makeSelectMotorListPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(MotorListPage);
