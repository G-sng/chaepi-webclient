/*
 *
 * MotorCaseCreation reducer
 *
 */
import produce from 'immer';
import {
  DEFAULT_ACTION,
  CASE_CREATE,
  RESET_REDUCER,
  UPLOAD_MULTIPLE_FILES,
  UPLOAD_PROGRESS,
  UPLOAD_TO_SIGNED_URL,
} from './constants';

export const initialState = {
  caseData: {},
  uploadInProgress: {},
  uploadImagesSuccess: false,
  totalImages: 0,
  imagesUploaded: 0,
};

/* eslint-disable default-case, no-param-reassign */
const motorCaseCreationReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case `${CASE_CREATE}_SUCCESS`:
        draft.caseData = action.value;
        draft.totalImages = action.value.urls.length;
        draft.imagesUploaded = 0;
        break;
      case RESET_REDUCER:
        draft.caseData = {};
        draft.uploadInProgress = {};
        draft.uploadImagesSuccess = false;
        draft.totalImages = 0;
        draft.imagesUploaded = 0;
        break;
      case `${UPLOAD_MULTIPLE_FILES}_SUCCESS`:
        draft.uploadImagesSuccess = true;
        break;
      case UPLOAD_PROGRESS:
        draft.uploadInProgress[action.key] = action.value.percent;
        break;
      case `${UPLOAD_TO_SIGNED_URL}_SUCCESS`:
        draft.imagesUploaded += 1;
        break;
    }
  });

export default motorCaseCreationReducer;
