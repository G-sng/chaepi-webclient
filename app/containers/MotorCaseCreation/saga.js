import request from 'superagent';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { getToken } from 'containers/Auth/saga';

import { BASE_URL } from 'utils/helper';
import {
  CASE_CREATE,
  UPLOAD_MULTIPLE_FILES,
  UPLOAD_PROGRESS,
  UPLOAD_TO_SIGNED_URL,
} from './constants';

export function* uploadMultipleFilesSaga(action) {
  const token = yield call(getToken);

  try {
    const data = yield request
      .post(`${BASE_URL}/cases`)
      .send({
        projectId: 1,
        expectedImages: action.value.length,
      })
      .set('Authorization', token)
      .then(response => {
        if (!response.ok) {
          throw new Error('Something Went Wrong');
        }
        return response.body;
      });

    yield put({
      type: `${CASE_CREATE}_SUCCESS`, // case creation is for creation of case, it maybe successfull without files being uploaded
      value: data,
    });

    const uploadImagesPromise = yield all(
      data.urls.map((item, key) =>
        call(
          uploadToSignedUrlSaga,
          item,
          action.value[key],
          data,
          key,
          action.dispatch,
        ),
      ),
    );
    yield put({
      type: `${UPLOAD_MULTIPLE_FILES}_SUCCESS`, // it is for success of all file upload
      value: uploadImagesPromise,
    });
  } catch (error) {
    console.error('error-----> ', error);
    alert(error.response.body.message);
    // if (error.status === 400) {
    //   console.error(error.response.body.reason);
    // }
    // yield put({ type: `${UPDATE_CASE_DETAILS}_FAILURE`, vehicle });
  }
}

export function* uploadToSignedUrlSaga(
  signedUrl,
  imageData,
  caseData,
  position,
  dispatch,
) {
  try {
    yield request
      .put(signedUrl)
      .send(imageData)
      .set('Content-Type', 'application/octet-stream')
      .on('progress', event => {
        /* the event is:
        {
          direction: "upload" or "download"
          percent: 0 to 100 // may be missing if file size is unknown
          total: // total file size, may be missing
          loaded: // bytes downloaded or uploaded so far
        } */
        dispatch({ type: UPLOAD_PROGRESS, key: position, value: event });
      })
      .then(response => {
        if (!response.ok) {
          throw new Error('Something Went Wrong');
        }
        return response.body;
      });

    yield put({ type: `${UPLOAD_TO_SIGNED_URL}_SUCCESS`, caseData, position });
  } catch (error) {
    console.error('error in upload to signed url-----> ', error);
    // alert(error.response.body.message);
    // if (error.status === 400) {
    //   console.error(error.response.body.reason);
    // }
    // yield put({ type: `${UPDATE_CASE_DETAILS}_FAILURE`, vehicle });
  }
}

export default function* motorCaseCreationSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(UPLOAD_MULTIPLE_FILES, uploadMultipleFilesSaga);
  yield takeLatest(UPLOAD_TO_SIGNED_URL, uploadToSignedUrlSaga);
}
