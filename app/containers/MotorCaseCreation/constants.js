/*
 *
 * MotorCaseCreation constants
 *
 */

export const DEFAULT_ACTION = 'app/MotorCaseCreation/DEFAULT_ACTION';
export const CASE_CREATE = 'app/MotorCaseCreation/CASE_CREATE';
export const RESET_REDUCER = 'app/MotorCaseCreation/RESET_REDUCER';
export const UPLOAD_MULTIPLE_FILES =
  'app/MotorCaseCreation/UPLOAD_MULTIPLE_FILES';
export const UPLOAD_PROGRESS = 'app/MotorCaseCreation/UPLOAD_PROGRESS';
export const UPLOAD_TO_SIGNED_URL =
  'app/MotorCaseCreation/UPLOAD_TO_SIGNED_URL';
