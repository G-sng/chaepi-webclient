/**
 *
 * Asynchronously loads the component for MotorCaseCreation
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
