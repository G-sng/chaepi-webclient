/**
 *
 * MotorCaseCreation
 *
 */

import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import LinearProgress from '@material-ui/core/LinearProgress';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectMotorCaseCreation from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { resetReducer, uploadMultipleFiles } from './actions';

const Wrapper = styled.div`
  .image-upload-status {
    font-size: 14px;
    padding: 0px 0px 20px;
  }
  .image-upload-progress {
    font-size: 14px;
    padding: 0px 0px 20px;
    max-height: 300px;
    overflow: auto;
  }
  .image-upload-progress-item {
    display: flex;
    margin: 5px 0px;
  }
`;

export function MotorCaseCreation({
  dispatch,
  handleCaseCreationSuccess,
  handleChangeCaseCreationDialogStatus,
  motorCaseCreation,
}) {
  useInjectReducer({ key: 'motorCaseCreation', reducer });
  useInjectSaga({ key: 'motorCaseCreation', saga });

  const uploadButtonRef = useRef(null);
  const {
    imagesUploaded,
    totalImages,
    uploadInProgress,
    uploadImagesSuccess,
  } = motorCaseCreation;

  useEffect(() => {
    return () => {
      dispatch(resetReducer());
    };
  }, []);

  // useEffect(() => {
  //   if (uploadImagesSuccess) {
  //     // if case created successfully, close dialog
  //     handleCaseCreationSuccess();
  //   }
  //   return () => {
  //     dispatch(resetReducer());
  //   };
  // }, [uploadImagesSuccess]);

  function handleClickDoneCaseCreation() {
    // if case created successfully, close dialog
    handleCaseCreationSuccess();
  }

  function handleUploadImagesClicked() {
    uploadButtonRef.current.click();
  }

  function handleMultipleFileUpload(e) {
    dispatch(uploadMultipleFiles(e.target.files, dispatch));
  }

  return (
    <Wrapper>
      <DialogTitle id="case-creation">
        <FormattedMessage {...messages.header} />
      </DialogTitle>
      <DialogContent>
        <div className="image-upload-status">
          Images Uploaded: {imagesUploaded}/{totalImages}
        </div>
        <div className="image-upload-progress">
          {Object.keys(uploadInProgress).map((item, key) => {
            if (uploadInProgress[item] === 100) {
              return (
                <div key={key} className="image-upload-progress-item">
                  <div>{parseInt(item, 10) + 1}:</div>
                  <div style={{ paddingLeft: '10px' }}>Upload Complete</div>
                </div>
              );
            }
            return (
              <div key={key} className="image-upload-progress-item">
                <div>{parseInt(item, 10) + 1}:</div>
                <div style={{ flex: 1, paddingLeft: '10px', marginTop: '6px' }}>
                  <LinearProgress
                    variant="determinate"
                    value={uploadInProgress[item]}
                  />
                </div>
              </div>
            );
          })}
        </div>
        {Object.keys(uploadInProgress).length === 0 && (
          <Button
            variant="contained"
            color="primary"
            onClick={handleUploadImagesClicked}
          >
            <FormattedMessage {...messages.uploadImages} />
          </Button>
        )}
        <input
          ref={uploadButtonRef}
          style={{ display: 'none' }}
          type="file"
          multiple
          name="upload-case-images"
          accept="image/*"
          onChange={handleMultipleFileUpload}
        />
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handleClickDoneCaseCreation}
          color="primary"
          disabled={!uploadImagesSuccess}
        >
          <FormattedMessage {...messages.create} />
        </Button>
        <Button
          onClick={handleChangeCaseCreationDialogStatus}
          color="secondary"
        >
          <FormattedMessage {...messages.close} />
        </Button>
      </DialogActions>
    </Wrapper>
  );
}

MotorCaseCreation.propTypes = {
  dispatch: PropTypes.func.isRequired,
  handleCaseCreationSuccess: PropTypes.func.isRequired,
  handleChangeCaseCreationDialogStatus: PropTypes.func.isRequired,
  motorCaseCreation: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  motorCaseCreation: makeSelectMotorCaseCreation(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(MotorCaseCreation);
