/*
 *
 * MotorCaseCreation actions
 *
 */

import {
  DEFAULT_ACTION,
  RESET_REDUCER,
  UPLOAD_MULTIPLE_FILES,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function uploadMultipleFiles(value, dispatch) {
  return {
    type: UPLOAD_MULTIPLE_FILES,
    value,
    dispatch,
  };
}

export function resetReducer() {
  return {
    type: RESET_REDUCER,
  };
}
