import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the motorCaseCreation state domain
 */

const selectMotorCaseCreationDomain = state =>
  state.motorCaseCreation || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by MotorCaseCreation
 */

const makeSelectMotorCaseCreation = () =>
  createSelector(
    selectMotorCaseCreationDomain,
    substate => substate,
  );

export default makeSelectMotorCaseCreation;
export { selectMotorCaseCreationDomain };
