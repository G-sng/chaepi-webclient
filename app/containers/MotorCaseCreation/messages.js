/*
 * MotorCaseCreation Messages
 *
 * This contains all the text for the MotorCaseCreation container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.MotorCaseCreation';

export default defineMessages({
  close: {
    id: `${scope}.close`,
    defaultMessage: 'Close',
  },
  create: {
    id: `${scope}.create`,
    defaultMessage: 'Done',
  },
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Create Case',
  },
  uploadImages: {
    id: `${scope}.uploadImages`,
    defaultMessage: 'Upload Images',
  },
});
