/**
 *
 * MotorDetailsPage
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import MotorActionPanel from 'components/MotorActionPanel';
import PageDetailsWrapper from 'components/PageDetailsWrapper';
import ThumbnailGallery from 'components/ThumbnailGallery';

import CanvasContainer from 'containers/CanvasContainer';
import Page from 'containers/Page';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectMotorDetailsPage from './selectors';
import reducer from './reducer';
import saga from './saga';
// import messages from './messages';
import {
  addDamageMarkings,
  addPartMarkings,
  addProfileMarkings,
  fetchMotorCaseDetails,
  fetchMotorDamagesSuggestions,
  fetchMotorMakeSuggestions,
  fetchMotorModelSuggestions,
  fetchMotorProfilesSuggestions,
  rejectImage,
  removeMotorDamagesSuggestion,
  removeMotorPartsSuggestion,
  saveDamagesFormValues,
  saveFormValues,
  savePartsFormValues,
  selectImage,
  setActiveTabValue,
  submitImage,
} from './actions';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  height: 100%;
`;

// const Header = styled.div`
//   display: flex;
//   line-height: 40px;
//   flex-flow: row nowrap;
//   justify-content: space-between;
//   border-bottom: 1px solid #11254b;
//   h4 {
//     margin: 0px;
//     font-weight: 400;
//   }
// `;

const Body = styled.div`
  display: flex;
  flex: 1;
  margin-top: 10px;
  height: 100%;
  .canvas-section {
    display: flex;
    flex-direction: column;
    // flex: 5;
    max-height: 100%;
    width: 75%;
    margin-right: 5px;
    background: #fcfcfc;
    border: 1px solid rgba(0, 0, 0, 0.12);
  }
  .action-section {
    // flex: 2;
    max-height: 100%;
    width: 25%;
    margin-left: 5px;
    background: #fff;
    border: 1px solid rgba(0, 0, 0, 0.12);
  }
`;

export function MotorDetailsPage({
  match: {
    params: { id },
  },
  motorDetailsPage,
  dispatch,
}) {
  useInjectReducer({ key: 'motorDetailsPage', reducer });
  useInjectSaga({ key: 'motorDetailsPage', saga });

  const {
    activeTabValue,
    motorCaseDetails,
    motorDamagesSuggestions,
    motorMakeSuggestions,
    motorModelSuggestions,
    motorPartsSuggestions,
    motorProfilesSuggestions,
    motorProfileForm,
    motorPartsSection,
    selectedImage,
  } = motorDetailsPage;

  useEffect(() => {
    // componentDidMount
    dispatch(fetchMotorCaseDetails(id, true));
    return () => {
      // componentWillUnmount
      dispatch(selectImage({}));
    };
  }, []);

  useEffect(() => {
    // componentWillReceiveProps
    // fetch profile when case details is loaded
    if (motorCaseDetails.projectId && motorProfilesSuggestions.length === 0) {
      dispatch(fetchMotorProfilesSuggestions());
    }
  }, [motorCaseDetails]);

  // function handleGetCanvasMarkings() {
  //   if (activeTabValue === 0) {
  //     // if currently on profile tab
  //     return selectedImage.profilePolygons;
  //   }
  //   if (activeTabValue === 1) {
  //     // if currently on profile tab
  //     return selectedImage.partsPolygons;
  //   }
  //   return selectedImage.damagesPolygons;
  // }

  return (
    <Page>
      <Helmet>
        <title>MotorDetailsPage</title>
        <meta name="description" content="Description of MotorDetailsPage" />
      </Helmet>
      <PageDetailsWrapper>
        <Wrapper>
          {/* <Header>
            <h4>{motorCaseDetails.caseNumber}</h4>
          </Header> */}
          <Body>
            <div className="canvas-section">
              <CanvasContainer selectedImage={selectedImage} />
              <ThumbnailGallery
                images={motorCaseDetails.images}
                handleSelectImage={val => dispatch(selectImage(val))}
                selectedImage={selectedImage}
              />
            </div>
            <div className="action-section">
              <MotorActionPanel
                activeTabValue={activeTabValue}
                caseNumber={motorCaseDetails.caseNumber}
                handleFetchMotorDamagesSuggestions={() =>
                  dispatch(fetchMotorDamagesSuggestions())
                }
                handleRemoveMotorDamagesSuggestion={(damage, key) =>
                  dispatch(removeMotorDamagesSuggestion(damage, key))
                }
                handleFetchMotorMakeSuggestions={text =>
                  dispatch(fetchMotorMakeSuggestions(text))
                }
                handleFetchMotorModelSuggestions={text =>
                  dispatch(fetchMotorModelSuggestions(text))
                }
                handleRemoveMotorPartsSuggestion={(part, key) =>
                  dispatch(removeMotorPartsSuggestion(part, key))
                }
                handleSaveDamagesFormValues={(name, val, key) =>
                  dispatch(saveDamagesFormValues(name, val, key))
                }
                handleSaveFormValues={(name, val) =>
                  dispatch(saveFormValues(name, val))
                }
                handleSavePartsFormValues={(name, val, key) =>
                  dispatch(savePartsFormValues(name, val, key))
                }
                handleSetActiveTabValue={val =>
                  dispatch(setActiveTabValue(val))
                }
                handleSubmitDamage={data =>
                  dispatch(addDamageMarkings(data, selectedImage))
                }
                handleSubmitImage={() => dispatch(submitImage(selectedImage))}
                handleSubmitPart={data =>
                  dispatch(addPartMarkings(data, selectedImage))
                }
                handleSubmitProfile={data =>
                  dispatch(addProfileMarkings(data, selectedImage))
                }
                handleSubmitRejectionReason={data =>
                  dispatch(rejectImage(data, selectedImage))
                }
                motorDamagesSuggestions={motorDamagesSuggestions}
                motorMakeSuggestions={motorMakeSuggestions}
                motorModelSuggestions={motorModelSuggestions}
                motorPartsSuggestions={motorPartsSuggestions}
                motorProfilesSuggestions={motorProfilesSuggestions}
                motorProfileForm={motorProfileForm}
                motorPartsSection={motorPartsSection}
                selectedImage={selectedImage}
              />
            </div>
          </Body>
        </Wrapper>
      </PageDetailsWrapper>
    </Page>
  );
}

MotorDetailsPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  motorDetailsPage: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  motorDetailsPage: makeSelectMotorDetailsPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(MotorDetailsPage);
