/* eslint-disable no-case-declarations */
/*
 *
 * MotorDetailsPage reducer
 *
 */
import produce from 'immer';
import { toTitleCase } from 'utils/helperFunctions';
import {
  DEFAULT_ACTION,
  FETCH_MOTOR_CASE_DETAILS,
  FETCH_MOTOR_DAMAGES_SUGGESTIONS,
  FETCH_MOTOR_DAMAGE_CLASSES,
  FETCH_MOTOR_MAKE_SUGGESTIONS,
  FETCH_MOTOR_MODEL_SUGGESTIONS,
  FETCH_MOTOR_PARTS_SUGGESTIONS,
  FETCH_MOTOR_PROFILES_SUGGESTIONS,
  REMOVE_MOTOR_DAMAGES_SUGGESTION,
  REMOVE_MOTOR_PARTS_SUGGESTION,
  SAVE_DAMAGES_FORM_VALUES,
  SAVE_FORM_VALUES,
  SAVE_PARTS_FORM_VALUES,
  SELECT_IMAGE,
  SET_ACTIVE_TAB_VALUE,
  UPDATE_POLYGONS,
} from './constants';

export const initialState = {
  motorCaseDetails: {
    id: null,
    images: [],
    caseNumber: null,
  },
  motorDamagesSuggestions: [],
  motorMakeSuggestions: [],
  motorModelSuggestions: [],
  motorPartsSuggestions: [],
  motorProfilesSuggestions: [],
  motorProfileForm: {
    make: null,
    model: null,
    profile: null,
    visibility: null,
  },
  motorPartsSection: {},
  selectedImage: {
    profilePolygons: [],
    partsPolygons: [],
    reversePartsPolygons: {}, // for parts markings, with partName as key
    damagesPolygons: [],
    reverseDamagesPolygons: {}, // for damages markings, with partName as key
    damageClasses: {},
  },
  activeTabValue: 0, // possible values -> 0(profile), 1(parts), 2(damages)
};

/* eslint-disable default-case, no-param-reassign */
const motorDetailsPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case `${FETCH_MOTOR_CASE_DETAILS}_SUCCESS`:
        draft.motorCaseDetails = action.value;
        break;
      case `${FETCH_MOTOR_DAMAGE_CLASSES}_SUCCESS`:
        draft.selectedImage.damageClasses[action.class] = action.value;
        break;
      case `${FETCH_MOTOR_DAMAGES_SUGGESTIONS}_SUCCESS`:
        draft.motorDamagesSuggestions = action.value;
        break;
      case `${FETCH_MOTOR_MAKE_SUGGESTIONS}_SUCCESS`:
        draft.motorMakeSuggestions = action.value;
        break;
      case `${FETCH_MOTOR_MODEL_SUGGESTIONS}_SUCCESS`:
        draft.motorModelSuggestions = action.value;
        break;
      case `${FETCH_MOTOR_PARTS_SUGGESTIONS}_SUCCESS`:
        const newSuggestions = action.value.map(item => ({
          ...item,
          motorPartFormValue: {
            visibility: null,
            isDamaged: false,
            isDamagedTextual: 'no',
          },
        }));
        draft.motorPartsSuggestions = [
          ...draft.motorPartsSuggestions,
          ...newSuggestions,
        ];
        break;
      case `${FETCH_MOTOR_PROFILES_SUGGESTIONS}_SUCCESS`:
        draft.motorProfilesSuggestions = action.value;
        break;
      case REMOVE_MOTOR_DAMAGES_SUGGESTION:
        draft.motorDamagesSuggestions = [
          ...draft.motorDamagesSuggestions.slice(0, action.key),
          ...draft.motorDamagesSuggestions.slice(action.key + 1),
        ];
        break;
      case REMOVE_MOTOR_PARTS_SUGGESTION:
        draft.motorPartsSuggestions = [
          ...draft.motorPartsSuggestions.slice(0, action.key),
          ...draft.motorPartsSuggestions.slice(action.key + 1),
        ];
        break;
      case SAVE_DAMAGES_FORM_VALUES:
        draft.motorDamagesSuggestions[action.key].motorDamageFormValue[
          action.name
        ] = action.value;
        break;
      case SAVE_FORM_VALUES:
        draft.motorProfileForm[action.name] = action.value;
        break;
      case SAVE_PARTS_FORM_VALUES:
        draft.motorPartsSuggestions[action.key].motorPartFormValue[
          action.name
        ] = action.value;
        break;
      case SELECT_IMAGE:
        draft.selectedImage = {
          profilePolygons: [],
          partsPolygons: [],
          reversePartsPolygons: {}, // for parts markings, with partName as key
          damagesPolygons: [],
          reverseDamagesPolygons: {}, // for parts markings, with partName as key
          damageClasses: {},
        };
        // reset values of motorProfileForm, as different image can have different make model etc
        draft.motorProfileForm.make = null;
        draft.motorProfileForm.model = null;
        draft.motorProfileForm.profile = null;
        draft.motorProfileForm.visibility = null;
        // reset values of suggestions
        draft.motorDamagesSuggestions = [];
        draft.motorMakeSuggestions = [];
        draft.motorModelSuggestions = [];
        draft.motorPartsSuggestions = [];
        // reset active Tab value
        draft.activeTabValue = 0;
        break;
      case `${SELECT_IMAGE}_SUCCESS`:
        draft.selectedImage = {
          ...action.value,
          // damage classes are populated before this reducer function is called, so we just copy those values
          damageClasses: draft.selectedImage.damageClasses,
        };
        if (
          action.value.profilePolygons &&
          action.value.profilePolygons.length > 0
        ) {
          draft.motorProfileForm.make = action.value.profilePolygons[0].make
            ? {
                value: action.value.profilePolygons[0].make,
                label: action.value.profilePolygons[0].make,
              }
            : null;
          draft.motorProfileForm.model = action.value.profilePolygons[0].model
            ? {
                value: action.value.profilePolygons[0].model,
                label: action.value.profilePolygons[0].model,
              }
            : null;
          draft.motorProfileForm.profile = action.value.profilePolygons[0].class
            ? {
                value: action.value.profilePolygons[0].class,
                label: toTitleCase(action.value.profilePolygons[0].class),
              }
            : null;
          draft.motorProfileForm.visibility = action.value.profilePolygons[0].visibility
            ? {
                value: action.value.profilePolygons[0].visibility,
                label: action.value.profilePolygons[0].visibility,
              }
            : null;
        }
        break;
      case SET_ACTIVE_TAB_VALUE:
        draft.activeTabValue = action.value;
        break;
      // case UPDATE_POLYGONS:
      //   draft.selectedImage = {
      //     ...draft.selectedImage,
      //     profilePolygons: [],
      //     partsPolygons: [],
      //     reversePartsPolygons: {}, // for parts markings, with partName as key
      //     damagesPolygons: [],
      //     reverseDamagesPolygons: {}, // for parts markings, with partName as key
      //   };
      //   break;
      case `${UPDATE_POLYGONS}_SUCCESS`:
        draft.selectedImage = {
          ...draft.selectedImage,
          ...action.value,
        };
        break;
    }
  });

export default motorDetailsPageReducer;
