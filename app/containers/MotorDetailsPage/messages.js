/*
 * MotorDetailsPage Messages
 *
 * This contains all the text for the MotorDetailsPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.MotorDetailsPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Case Details',
  },
});
