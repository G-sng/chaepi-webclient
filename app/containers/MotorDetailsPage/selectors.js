import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the motorDetailsPage state domain
 */

const selectMotorDetailsPageDomain = state =>
  state.motorDetailsPage || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by MotorDetailsPage
 */

const makeSelectMotorDetailsPage = () =>
  createSelector(
    selectMotorDetailsPageDomain,
    substate => substate,
  );

export default makeSelectMotorDetailsPage;
export { selectMotorDetailsPageDomain };
