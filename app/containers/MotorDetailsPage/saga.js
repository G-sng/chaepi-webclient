/* eslint-disable no-unused-expressions */
import request from 'superagent';
import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import { getToken } from 'containers/Auth/saga';
import { removeCompletedPolygons } from 'containers/CanvasContainer/actions';

import { BASE_URL } from 'utils/helper';
import {
  ADD_DAMAGE_MARKINGS,
  ADD_PART_MARKINGS,
  ADD_PROFILE_MARKINGS,
  FETCH_MOTOR_CASE_DETAILS,
  FETCH_MOTOR_DAMAGE_CLASSES,
  FETCH_MOTOR_DAMAGES_SUGGESTIONS,
  FETCH_MOTOR_MAKE_SUGGESTIONS,
  FETCH_MOTOR_MODEL_SUGGESTIONS,
  FETCH_MOTOR_PARTS_SUGGESTIONS,
  FETCH_MOTOR_PROFILES_SUGGESTIONS,
  REJECT_IMAGE,
  SELECT_IMAGE,
  SET_ACTIVE_TAB_VALUE,
  SUBMIT_IMAGE,
  UPDATE_POLYGONS,
} from './constants';
import { fetchMotorPartsSuggestions } from './actions';

export function* addDamageMarkingsSaga(action) {
  const token = yield call(getToken);
  const state = yield select();
  const {
    canvasContainer: { completedPolygons },
  } = state;

  if (completedPolygons.length === 0) {
    alert('Please draw the Polygon');
  } else {
    const reqData = {
      markingType: 'damage',
      marking: {
        boundingBox: {
          polygons: completedPolygons,
        },
        action: action.value.action.value,
        class: action.value.class.value,
        parentClass: action.value.parentClass,
        severity: action.value.severity.value,
      },
    };
    try {
      const data = yield request
        .post(
          `${BASE_URL}/cases/${action.selectedImage.caseId}/images/${
            action.selectedImage.id
          }/markings`,
        )
        .send(reqData)
        .set('Authorization', token)
        .then(response => {
          if (!response.ok) {
            throw new Error('Something Went Wrong');
          }
          return response.body;
        });

      alert('Damage saved');
      yield put({
        type: `${ADD_PART_MARKINGS}_SUCCESS`,
        value: data,
      });
      yield put(removeCompletedPolygons());
      // yield put({ type: SET_ACTIVE_TAB_VALUE, value: 1 }); // switch to parts section
      // yield put(fetchMotorPartsSuggestions(action.value.profile.value));
      yield put({
        type: FETCH_MOTOR_CASE_DETAILS,
        value: action.selectedImage.caseId,
        isFirstRender: false,
      });
    } catch (error) {
      console.error('error-----> ', error);
      alert(error.response.body.message);
      // if (error.status === 400) {
      //   console.error(error.response.body.reason);
      // }
      // yield put({ type: `${UPDATE_CASE_DETAILS}_FAILURE`, vehicle });
    }
  }
}

export function* addPartMarkingsSaga(action) {
  const token = yield call(getToken);
  const state = yield select();
  const {
    canvasContainer: { completedPolygons },
  } = state;
  if (completedPolygons.length === 0) {
    alert('Please draw the Polygon');
  } else {
    const reqData = {
      markingType: 'part',
      marking: {
        boundingBox: {
          polygons: completedPolygons,
        },
        visibility: action.value.visibility.value,
        class: action.value.class,
        isDamaged: action.value.isDamagedTextual === 'yes',
      },
    };
    try {
      const data = yield request
        .post(
          `${BASE_URL}/cases/${action.selectedImage.caseId}/images/${
            action.selectedImage.id
          }/markings`,
        )
        .send(reqData)
        .set('Authorization', token)
        .then(response => {
          if (!response.ok) {
            throw new Error('Something Went Wrong');
          }
          return response.body;
        });

      alert('Part saved');
      yield put({
        type: `${ADD_PART_MARKINGS}_SUCCESS`,
        value: data,
      });
      yield put(removeCompletedPolygons());
      // yield put({ type: SET_ACTIVE_TAB_VALUE, value: 1 }); // switch to parts section
      // yield put(fetchMotorPartsSuggestions(action.value.profile.value));
      yield put({
        type: FETCH_MOTOR_CASE_DETAILS,
        value: action.selectedImage.caseId,
        isFirstRender: false,
      });
    } catch (error) {
      console.error('error-----> ', error);
      alert(error.response.body.message);
      // if (error.status === 400) {
      //   console.error(error.response.body.reason);
      // }
      // yield put({ type: `${UPDATE_CASE_DETAILS}_FAILURE`, vehicle });
    }
  }
}

export function* addProfileMarkingsSaga(action) {
  const token = yield call(getToken);
  const state = yield select();
  const {
    canvasContainer: { completedPolygons, imageHeight, imageWidth },
  } = state;
  if (completedPolygons.length === 0) {
    alert('Please draw the Polygon');
  } else {
    const reqData = {
      markingType: 'profile',
      height: imageHeight || '',
      width: imageWidth || '',
      marking: {
        make: action.value.make.value || undefined,
        model: action.value.model.value || undefined,
        boundingBox: {
          polygons: completedPolygons,
        },
        visibility: action.value.visibility.value,
        class: action.value.profile.value,
      },
    };
    try {
      const data = yield request
        .post(
          `${BASE_URL}/cases/${action.selectedImage.caseId}/images/${
            action.selectedImage.id
          }/markings`,
        )
        .send(reqData)
        .set('Authorization', token)
        .then(response => {
          if (!response.ok) {
            throw new Error('Something Went Wrong');
          }
          return response.body;
        });

      alert('Profile saved');
      yield put({
        type: `${ADD_PROFILE_MARKINGS}_SUCCESS`,
        value: data,
      });
      yield put(removeCompletedPolygons());
      yield put({ type: SET_ACTIVE_TAB_VALUE, value: 1 }); // switch to parts section
      yield put(fetchMotorPartsSuggestions(action.value.profile.value));
      yield put({
        type: FETCH_MOTOR_CASE_DETAILS,
        value: action.selectedImage.caseId,
        isFirstRender: false,
      });
    } catch (error) {
      console.error('error-----> ', error);
      alert(error.response.body.message);
      // if (error.status === 400) {
      //   console.error(error.response.body.reason);
      // }
      // yield put({ type: `${UPDATE_CASE_DETAILS}_FAILURE`, vehicle });
    }
  }
}

export function* fetchMotorCaseDetailsSaga(action) {
  const token = yield call(getToken);
  const state = yield select();
  const {
    motorDetailsPage: {
      selectedImage: { id },
    },
  } = state;

  try {
    const data = yield request
      .get(`${BASE_URL}/cases/${action.value}`)
      .set('Authorization', token)
      .then(response => {
        if (!response.ok) {
          throw new Error('Something Went Wrong');
        }
        return response.body;
      });

    yield put({
      type: `${FETCH_MOTOR_CASE_DETAILS}_SUCCESS`,
      value: data,
    });
    if (action.isFirstRender) {
      // use the first image from the images array
      yield put({
        type: SELECT_IMAGE,
        value: data.images[0] || {},
      });
    } else {
      // find the current image in the new data and use the new data
      let currentImage = {};
      data.images.map(item => {
        if (item.id === id) {
          currentImage = item;
        }
        return item;
      });
      yield put({
        type: UPDATE_POLYGONS,
        value: currentImage,
      });
    }
  } catch (error) {
    console.error('error-----> ', error);
    alert(error.response.body.message);
    // if (error.status === 400) {
    //   console.error(error.response.body.reason);
    // }
    // yield put({ type: `${UPDATE_CASE_DETAILS}_FAILURE`, vehicle });
  }
}

export function* fetchMotorDamageClassesSaga(action) {
  const token = yield call(getToken);
  const state = yield select();
  const {
    motorDetailsPage: {
      motorCaseDetails: { projectId },
    },
  } = state;

  try {
    const data = yield request
      .get(
        `${BASE_URL}/projects/${projectId}/classes?type=damage&parentClass=${
          action.value
        }`,
      )
      .set('Authorization', token)
      .then(response => {
        if (!response.ok) {
          throw new Error('Something Went Wrong');
        }
        return response.body;
      });

    yield put({
      type: `${FETCH_MOTOR_DAMAGE_CLASSES}_SUCCESS`,
      value: data.classes,
      class: action.value,
    });
  } catch (error) {
    console.error('error-----> ', error);
    alert(error.response.body.message);
    // if (error.status === 400) {
    //   console.error(error.response.body.reason);
    // }
    // yield put({ type: `${UPDATE_CASE_DETAILS}_FAILURE`, vehicle });
  }
}

export function* fetchMotorDamagesSuggestionsSaga() {
  const state = yield select();
  const {
    motorDetailsPage: {
      selectedImage: { partsPolygons },
    },
  } = state;

  const damagedParts = partsPolygons.reduce((acc, item) => {
    if (item.isDamaged) {
      acc.push({
        ...item,
        motorDamageFormValue: {
          damageType: null,
          severity: null,
          action: null,
        },
      });
    }
    return acc;
  }, []);

  yield put({
    type: `${FETCH_MOTOR_DAMAGES_SUGGESTIONS}_SUCCESS`,
    value: damagedParts,
  });
}

export function* fetchMotorMakeSuggestionsSaga(action) {
  const token = yield call(getToken);
  const state = yield select();
  const {
    motorDetailsPage: {
      motorCaseDetails: { projectId },
    },
  } = state;

  if (action.value && action.value.trim().length > 0) {
    try {
      const data = yield request
        .get(`${BASE_URL}/projects/${projectId}/makes?q=${action.value.trim()}`)
        .set('Authorization', token)
        .then(response => {
          if (!response.ok) {
            throw new Error('Something Went Wrong');
          }
          return response.body;
        });

      yield put({
        type: `${FETCH_MOTOR_MAKE_SUGGESTIONS}_SUCCESS`,
        value: data.makes,
      });
    } catch (error) {
      console.error('error-----> ', error);
      alert(error.response.body.message);
      // if (error.status === 400) {
      //   console.error(error.response.body.reason);
      // }
      // yield put({ type: `${UPDATE_CASE_DETAILS}_FAILURE`, vehicle });
    }
  }
}

export function* fetchMotorModelSuggestionsSaga(action) {
  const token = yield call(getToken);
  const state = yield select();
  const {
    motorDetailsPage: {
      motorCaseDetails: { projectId },
      motorProfileForm: { make },
    },
  } = state;

  if (make && make.value && action.value && action.value.trim().length > 0) {
    try {
      const data = yield request
        .get(
          `${BASE_URL}/projects/${projectId}/models?make=${make.value}&q=${
            action.value
          }`,
        )
        .set('Authorization', token)
        .then(response => {
          if (!response.ok) {
            throw new Error('Something Went Wrong');
          }
          return response.body;
        });

      yield put({
        type: `${FETCH_MOTOR_MODEL_SUGGESTIONS}_SUCCESS`,
        value: data.models,
      });
    } catch (error) {
      console.error('error-----> ', error);
      alert(error.response.body.message);
      // if (error.status === 400) {
      //   console.error(error.response.body.reason);
      // }
      // yield put({ type: `${UPDATE_CASE_DETAILS}_FAILURE`, vehicle });
    }
  }
}

export function* fetchMotorPartsSuggestionsSaga(action) {
  const token = yield call(getToken);
  const state = yield select();
  const {
    motorDetailsPage: {
      motorCaseDetails: { projectId },
    },
  } = state;

  try {
    const data = yield request
      .get(
        `${BASE_URL}/projects/${projectId}/classes?type=part&parentClass=${
          action.value
        }`,
      )
      .set('Authorization', token)
      .then(response => {
        if (!response.ok) {
          throw new Error('Something Went Wrong');
        }
        return response.body;
      });

    yield put({
      type: `${FETCH_MOTOR_PARTS_SUGGESTIONS}_SUCCESS`,
      value: data.classes,
    });
  } catch (error) {
    console.error('error-----> ', error);
    alert(error.response.body.message);
    // if (error.status === 400) {
    //   console.error(error.response.body.reason);
    // }
    // yield put({ type: `${UPDATE_CASE_DETAILS}_FAILURE`, vehicle });
  }
}

export function* fetchMotorProfilesSuggestionsSaga() {
  const token = yield call(getToken);
  const state = yield select();
  const {
    motorDetailsPage: {
      motorCaseDetails: { projectId },
    },
  } = state;

  try {
    const data = yield request
      .get(`${BASE_URL}/projects/${projectId}/classes?type=profile`)
      .set('Authorization', token)
      .then(response => {
        if (!response.ok) {
          throw new Error('Something Went Wrong');
        }
        return response.body;
      });

    yield put({
      type: `${FETCH_MOTOR_PROFILES_SUGGESTIONS}_SUCCESS`,
      value: data.classes,
    });
  } catch (error) {
    console.error('error-----> ', error);
    alert(error.response.body.message);
    // if (error.status === 400) {
    //   console.error(error.response.body.reason);
    // }
    // yield put({ type: `${UPDATE_CASE_DETAILS}_FAILURE`, vehicle });
  }
}

export function* updatePolygonsSaga(action) {
  const newSelectedImage = {
    markings: action.value.markings,
    profilePolygons: [],
    partsPolygons: [],
    reversePartsPolygons: {},
    damagesPolygons: [],
    reverseDamagesPolygons: {},
  };
  const fetchDamageClasses = [];

  // split markings into 3 -> Profile, Parts and Damages
  action.value.markings &&
    action.value.markings.map(item => {
      if (item.type === 'profile') {
        newSelectedImage.profilePolygons.push(item);
      } else if (item.type === 'part') {
        newSelectedImage.partsPolygons.push(item);
        if (newSelectedImage.reversePartsPolygons[item.class]) {
          // if there already exists markings for the part
          newSelectedImage.reversePartsPolygons[item.class] = [
            ...newSelectedImage.reversePartsPolygons[item.class],
            item,
          ];
        } else {
          // if it is the first marking for the part
          newSelectedImage.reversePartsPolygons[item.class] = [item];
        }
        // fetch damage classes for damages parts
        if (item.isDamaged) {
          fetchDamageClasses.push(item);
        }
      } else if (item.type === 'damage') {
        newSelectedImage.damagesPolygons.push(item);
        if (newSelectedImage.reverseDamagesPolygons[item.parentClass]) {
          // if there already exists markings for the damage
          newSelectedImage.reverseDamagesPolygons[item.parentClass] = [
            ...newSelectedImage.reverseDamagesPolygons[item.parentClass],
            item,
          ];
        } else {
          // if it is the first marking for the damage
          newSelectedImage.reverseDamagesPolygons[item.parentClass] = [item];
        }
      }
      return item;
    });

  // fetch possible damage classes for the damaged parts
  yield all(
    fetchDamageClasses.map(item =>
      call(fetchMotorDamageClassesSaga, {
        type: FETCH_MOTOR_DAMAGE_CLASSES,
        value: item.class,
      }),
    ),
  );
  yield put({ type: `${UPDATE_POLYGONS}_SUCCESS`, value: newSelectedImage });
}

export function* rejectImageSaga(action) {
  const token = yield call(getToken);
  try {
    const data = yield request
      .put(
        `${BASE_URL}/cases/${action.selectedImage.caseId}/images/${
          action.selectedImage.id
        }/reject`,
      )
      .send({
        reason: action.value,
      })
      .set('Authorization', token)
      .then(response => {
        if (!response.ok) {
          throw new Error('Something Went Wrong');
        }
        return response.body;
      });

    console.log(data);
    alert('Image Rejected Successfully');
  } catch (error) {
    console.error('error-----> ', error);
    alert(error.response.body.message);
    // if (error.status === 400) {
    //   console.error(error.response.body.reason);
    // }
    // yield put({ type: `${UPDATE_CASE_DETAILS}_FAILURE`, vehicle });
  }
}

export function* selectImageSaga(action) {
  const newSelectedImage = {
    ...action.value,
    profilePolygons: [],
    partsPolygons: [],
    reversePartsPolygons: {},
    damagesPolygons: [],
    reverseDamagesPolygons: {},
    damageClasses: {},
  };
  const fetchDamageClasses = [];

  // split markings into 3 -> Profile, Parts and Damages
  action.value.markings &&
    action.value.markings.map(item => {
      if (item.type === 'profile') {
        newSelectedImage.profilePolygons.push(item);
      } else if (item.type === 'part') {
        newSelectedImage.partsPolygons.push(item);
        if (newSelectedImage.reversePartsPolygons[item.class]) {
          // if there already exists markings for the part
          newSelectedImage.reversePartsPolygons[item.class] = [
            ...newSelectedImage.reversePartsPolygons[item.class],
            item,
          ];
        } else {
          // if it is the first marking for the part
          newSelectedImage.reversePartsPolygons[item.class] = [item];
        }
        // fetch damage classes for damages parts
        if (item.isDamaged) {
          fetchDamageClasses.push(item);
        }
      } else if (item.type === 'damage') {
        newSelectedImage.damagesPolygons.push(item);
        if (newSelectedImage.reverseDamagesPolygons[item.parentClass]) {
          // if there already exists markings for the damage
          newSelectedImage.reverseDamagesPolygons[item.parentClass] = [
            ...newSelectedImage.reverseDamagesPolygons[item.parentClass],
            item,
          ];
        } else {
          // if it is the first marking for the damage
          newSelectedImage.reverseDamagesPolygons[item.parentClass] = [item];
        }
      }
      return item;
    });

  // fetch possible parts for the given profiles
  yield all(
    newSelectedImage.profilePolygons.map(item =>
      call(fetchMotorPartsSuggestionsSaga, {
        type: FETCH_MOTOR_PARTS_SUGGESTIONS,
        value: item.class,
      }),
    ),
  );
  // fetch possible damage classes for the damaged parts
  yield all(
    fetchDamageClasses.map(item =>
      call(fetchMotorDamageClassesSaga, {
        type: FETCH_MOTOR_DAMAGE_CLASSES,
        value: item.class,
      }),
    ),
  );
  yield put({ type: `${SELECT_IMAGE}_SUCCESS`, value: newSelectedImage });
}

export function* submitImageSaga(action) {
  const token = yield call(getToken);
  try {
    const data = yield request
      .put(
        `${BASE_URL}/cases/${action.value.caseId}/images/${
          action.value.id
        }/submit`,
      )
      .set('Authorization', token)
      .then(response => {
        if (!response.ok) {
          throw new Error('Something Went Wrong');
        }
        return response.body;
      });

    console.log(data);
    alert('Image submitted');
  } catch (error) {
    console.error('error-----> ', error);
    alert(error.response.body.message);
    // if (error.status === 400) {
    //   console.error(error.response.body.reason);
    // }
    // yield put({ type: `${UPDATE_CASE_DETAILS}_FAILURE`, vehicle });
  }
}

export default function* motorDetailsPageSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(ADD_DAMAGE_MARKINGS, addDamageMarkingsSaga);
  yield takeLatest(ADD_PART_MARKINGS, addPartMarkingsSaga);
  yield takeLatest(ADD_PROFILE_MARKINGS, addProfileMarkingsSaga);
  yield takeLatest(FETCH_MOTOR_CASE_DETAILS, fetchMotorCaseDetailsSaga);
  yield takeLatest(FETCH_MOTOR_DAMAGE_CLASSES, fetchMotorDamageClassesSaga);
  yield takeLatest(
    FETCH_MOTOR_DAMAGES_SUGGESTIONS,
    fetchMotorDamagesSuggestionsSaga,
  );
  yield takeLatest(FETCH_MOTOR_MAKE_SUGGESTIONS, fetchMotorMakeSuggestionsSaga);
  yield takeLatest(
    FETCH_MOTOR_MODEL_SUGGESTIONS,
    fetchMotorModelSuggestionsSaga,
  );
  yield takeLatest(
    FETCH_MOTOR_PARTS_SUGGESTIONS,
    fetchMotorPartsSuggestionsSaga,
  );
  yield takeLatest(
    FETCH_MOTOR_PROFILES_SUGGESTIONS,
    fetchMotorProfilesSuggestionsSaga,
  );
  yield takeLatest(REJECT_IMAGE, rejectImageSaga);
  yield takeLatest(SELECT_IMAGE, selectImageSaga);
  yield takeLatest(SUBMIT_IMAGE, submitImageSaga);
  yield takeLatest(UPDATE_POLYGONS, updatePolygonsSaga);
}
