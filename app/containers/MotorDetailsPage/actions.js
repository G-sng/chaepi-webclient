/*
 *
 * MotorDetailsPage actions
 *
 */

import {
  DEFAULT_ACTION,
  ADD_DAMAGE_MARKINGS,
  ADD_PART_MARKINGS,
  ADD_PROFILE_MARKINGS,
  FETCH_MOTOR_CASE_DETAILS,
  FETCH_MOTOR_DAMAGES_SUGGESTIONS,
  FETCH_MOTOR_MAKE_SUGGESTIONS,
  FETCH_MOTOR_MODEL_SUGGESTIONS,
  FETCH_MOTOR_PARTS_SUGGESTIONS,
  FETCH_MOTOR_PROFILES_SUGGESTIONS,
  REJECT_IMAGE,
  REMOVE_MOTOR_DAMAGES_SUGGESTION,
  REMOVE_MOTOR_PARTS_SUGGESTION,
  SAVE_DAMAGES_FORM_VALUES,
  SAVE_FORM_VALUES,
  SAVE_PARTS_FORM_VALUES,
  SELECT_IMAGE,
  SET_ACTIVE_TAB_VALUE,
  SUBMIT_IMAGE,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function addDamageMarkings(value, selectedImage) {
  return {
    type: ADD_DAMAGE_MARKINGS,
    value,
    selectedImage,
  };
}

export function addPartMarkings(value, selectedImage) {
  return {
    type: ADD_PART_MARKINGS,
    value,
    selectedImage,
  };
}

export function addProfileMarkings(value, selectedImage) {
  return {
    type: ADD_PROFILE_MARKINGS,
    value,
    selectedImage,
  };
}

export function fetchMotorCaseDetails(value, isFirstRender) {
  return {
    type: FETCH_MOTOR_CASE_DETAILS,
    value,
    isFirstRender,
  };
}

export function fetchMotorDamagesSuggestions() {
  return {
    type: FETCH_MOTOR_DAMAGES_SUGGESTIONS,
  };
}

export function fetchMotorMakeSuggestions(value) {
  return {
    type: FETCH_MOTOR_MAKE_SUGGESTIONS,
    value,
  };
}

export function fetchMotorModelSuggestions(value) {
  return {
    type: FETCH_MOTOR_MODEL_SUGGESTIONS,
    value,
  };
}

export function fetchMotorPartsSuggestions(value) {
  return {
    type: FETCH_MOTOR_PARTS_SUGGESTIONS,
    value,
  };
}

export function fetchMotorProfilesSuggestions() {
  return {
    type: FETCH_MOTOR_PROFILES_SUGGESTIONS,
  };
}

export function rejectImage(value, selectedImage) {
  return {
    type: REJECT_IMAGE,
    value,
    selectedImage,
  };
}

export function removeMotorDamagesSuggestion(value, key) {
  return {
    type: REMOVE_MOTOR_DAMAGES_SUGGESTION,
    value,
    key,
  };
}

export function removeMotorPartsSuggestion(value, key) {
  return {
    type: REMOVE_MOTOR_PARTS_SUGGESTION,
    value,
    key,
  };
}

export function saveDamagesFormValues(name, value, key) {
  return {
    type: SAVE_DAMAGES_FORM_VALUES,
    name,
    value,
    key,
  };
}

export function saveFormValues(name, value) {
  return {
    type: SAVE_FORM_VALUES,
    name,
    value,
  };
}

export function savePartsFormValues(name, value, key) {
  return {
    type: SAVE_PARTS_FORM_VALUES,
    name,
    value,
    key,
  };
}

export function selectImage(value) {
  return {
    type: SELECT_IMAGE,
    value,
  };
}

export function setActiveTabValue(value) {
  return {
    type: SET_ACTIVE_TAB_VALUE,
    value,
  };
}

export function submitImage(value) {
  return {
    type: SUBMIT_IMAGE,
    value,
  };
}
