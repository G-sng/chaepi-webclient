/*
 *
 * MotorDetailsPage constants
 *
 */

export const DEFAULT_ACTION = 'app/MotorDetailsPage/DEFAULT_ACTION';
export const ADD_DAMAGE_MARKINGS = 'app/MotorDetailsPage/ADD_DAMAGE_MARKINGS';
export const ADD_PART_MARKINGS = 'app/MotorDetailsPage/ADD_PART_MARKINGS';
export const ADD_PROFILE_MARKINGS = 'app/MotorDetailsPage/ADD_PROFILE_MARKINGS';
export const FETCH_MOTOR_CASE_DETAILS =
  'app/MotorDetailsPage/FETCH_MOTOR_CASE_DETAILS';
export const FETCH_MOTOR_DAMAGES_SUGGESTIONS =
  'app/MotorDetailsPage/FETCH_MOTOR_DAMAGES_SUGGESTIONS';
export const FETCH_MOTOR_DAMAGE_CLASSES =
  'app/MotorDetailsPage/FETCH_MOTOR_DAMAGE_CLASSES';
export const FETCH_MOTOR_MAKE_SUGGESTIONS =
  'app/MotorDetailsPage/FETCH_MOTOR_MAKE_SUGGESTIONS';
export const FETCH_MOTOR_MODEL_SUGGESTIONS =
  'app/MotorDetailsPage/FETCH_MOTOR_MODEL_SUGGESTIONS';
export const FETCH_MOTOR_PARTS_SUGGESTIONS =
  'app/MotorDetailsPage/FETCH_MOTOR_PARTS_SUGGESTIONS';
export const FETCH_MOTOR_PROFILES_SUGGESTIONS =
  'app/MotorDetailsPage/FETCH_MOTOR_PROFILES_SUGGESTIONS';
export const REJECT_IMAGE = 'app/MotorDetailsPage/REJECT_IMAGE';
export const REMOVE_MOTOR_DAMAGES_SUGGESTION =
  'app/MotorDetailsPage/REMOVE_MOTOR_DAMAGES_SUGGESTION';
export const REMOVE_MOTOR_PARTS_SUGGESTION =
  'app/MotorDetailsPage/REMOVE_MOTOR_PARTS_SUGGESTION';
export const SAVE_DAMAGES_FORM_VALUES =
  'app/MotorDetailsPage/SAVE_DAMAGES_FORM_VALUES';
export const SAVE_FORM_VALUES = 'app/MotorDetailsPage/SAVE_FORM_VALUES';
export const SAVE_PARTS_FORM_VALUES =
  'app/MotorDetailsPage/SAVE_PARTS_FORM_VALUES';
export const SELECT_IMAGE = 'app/MotorDetailsPage/SELECT_IMAGE';
export const SET_ACTIVE_TAB_VALUE = 'app/MotorDetailsPage/SET_ACTIVE_TAB_VALUE';
export const SUBMIT_IMAGE = 'app/MotorDetailsPage/SUBMIT_IMAGE';
export const UPDATE_POLYGONS = 'app/MotorDetailsPage/UPDATE_POLYGONS';
