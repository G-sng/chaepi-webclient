/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import { useEffect } from 'react';
// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

export default function HomePage({ history }) {
  useEffect(() => {
    history.push('/project');
  }, []);

  return null;
}
