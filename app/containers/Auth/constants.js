/*
 *
 * Auth constants
 *
 */

export const DEFAULT_ACTION = 'app/Auth/DEFAULT_ACTION';
export const LOGOUT = 'app/Auth/LOGOUT';
export const SYNC_STORAGE = 'app/Auth/SYNC_STORAGE';
