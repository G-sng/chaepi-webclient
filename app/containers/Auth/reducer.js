/*
 *
 * Auth reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION, LOGOUT, SYNC_STORAGE } from './constants';

export const initialState = {
  error: {},
  loaded: false,
  isAuthenticated: false,
  user: {},
};

/* eslint-disable default-case, no-param-reassign */
const authReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case LOGOUT:
        draft.isAuthenticated = false;
        break;
      case `${SYNC_STORAGE}_SUCCESS`:
        draft.error = {};
        draft.loaded = true;
        draft.isAuthenticated = true;
        draft.user = action.data[4];
        break;
      case `${SYNC_STORAGE}_FAILURE`:
        if (action.error.message === 'sync_auth_from_storage_failed') {
          draft.error = {};
          draft.loaded = true;
          draft.isAuthenticated = false;
          break;
        }
        draft.error = action.error;
        draft.loaded = true;
        draft.isAuthenticated = false;
        break;
    }
  });

export default authReducer;
