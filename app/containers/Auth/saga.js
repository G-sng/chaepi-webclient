import { all, call, put, takeLatest } from 'redux-saga/effects';
import localForage from 'localforage';

import { LOGOUT, SYNC_STORAGE } from './constants';

function* clearAuthCache(key) {
  yield localForage.removeItem(key);
}

export function* getToken() {
  return yield localForage.getItem('chaepi@accessToken');
}

export function* loginFlow(response) {
  yield all(
    Object.keys(response).map(key =>
      call(setAuthCache, `chaepi@${key}`, response[key]),
    ),
  );
}

export function* logoutSaga(action) {
  const keys = yield localForage.keys();
  const matchedKeys = keys.filter(key => key.startsWith(`chaepi@`));

  if (matchedKeys.length) {
    yield all(matchedKeys.map(key => call(clearAuthCache, key)));
  }
  action.history.push('/login');
}

function* setAuthCache(key, value) {
  yield localForage.setItem(key, value);
}

export function* syncStorageAuthSaga() {
  try {
    const keys = yield localForage.keys();
    const matchedKeys = keys.filter(key => key.startsWith(`chaepi@`));

    if (!matchedKeys.length) {
      throw new Error('sync_auth_from_storage_failed');
    }

    const matchedValues = yield all(
      matchedKeys.map(key => localForage.getItem(key)),
    );
    yield put({ type: `${SYNC_STORAGE}_SUCCESS`, data: matchedValues });
  } catch (error) {
    yield put({ type: `${SYNC_STORAGE}_FAILURE`, error });
  }
}

// Individual exports for testing
export default function* authSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(LOGOUT, logoutSaga);
  yield takeLatest(SYNC_STORAGE, syncStorageAuthSaga);
}
