/*
 *
 * Auth actions
 *
 */

import { DEFAULT_ACTION, LOGOUT, SYNC_STORAGE } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function logout(history) {
  return {
    type: LOGOUT,
    history,
  };
}

export function syncStorage() {
  return {
    type: SYNC_STORAGE,
  };
}
